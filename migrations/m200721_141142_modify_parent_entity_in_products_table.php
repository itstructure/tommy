<?php

use yii\db\Migration;

/**
 * Class m200721_141142_modify_columns_in_products_table
 */
class m200721_141142_modify_parent_entity_in_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-products-pageId',
            'products'
        );

        $this->dropIndex(
            'idx-products-pageId',
            'products'
        );

        $this->dropColumn('products', 'pageId');

        $this->addColumn('products', 'categoryId', $this->integer());

        $this->createIndex(
            'idx-products-categoryId',
            'products',
            'categoryId'
        );

        $this->addForeignKey(
            'fk-products-categoryId',
            'products',
            'categoryId',
            'categories',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-products-categoryId',
            'products'
        );

        $this->dropIndex(
            'idx-products-categoryId',
            'products'
        );

        $this->renameColumn('products', 'categoryId', 'pageId');

        $this->createIndex(
            'idx-products-pageId',
            'products',
            'pageId'
        );

        $this->addForeignKey(
            'fk-products-pageId',
            'products',
            'pageId',
            'pages',
            'id',
            'SET NULL'
        );
    }
}
