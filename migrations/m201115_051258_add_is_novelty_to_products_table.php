<?php

use yii\db\Migration;

/**
 * Class m201115_051258_add_is_novelty_to_products_table
 */
class m201115_051258_add_is_novelty_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'isNovelty', $this->tinyInteger(1)->notNull()->defaultValue(0));

        $this->createIndex(
            'idx-products-isNovelty',
            'products',
            'isNovelty'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-products-isNovelty',
            'products'
        );

        $this->dropColumn('products', 'isNovelty');
    }
}
