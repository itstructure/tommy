<?php

use yii\db\Migration;

/**
 * Class m201002_143449_add_page_id_to_contacts_table
 */
class m201002_143449_add_page_id_to_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contacts', 'pageId', $this->integer());

        $this->createIndex(
            'idx-contacts-pageId',
            'contacts',
            'pageId'
        );

        $this->addForeignKey(
            'fk-contacts-pageId',
            'contacts',
            'pageId',
            'pages',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-contacts-pageId',
            'contacts'
        );

        $this->dropIndex(
            'idx-contacts-pageId',
            'contacts'
        );

        $this->dropColumn('contacts', 'pageId');
    }
}
