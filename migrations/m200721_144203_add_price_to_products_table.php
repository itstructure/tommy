<?php

use yii\db\Migration;

/**
 * Class m200721_144203_add_price_to_products_table
 */
class m200721_144203_add_price_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'price', $this->float(2));

        $this->createIndex(
            'idx-products-price',
            'products',
            'price'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-products-price',
            'products'
        );

        $this->dropColumn('products', 'price');
    }
}
