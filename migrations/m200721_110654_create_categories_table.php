<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories`.
 */
class m200721_110654_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categories',
            [
                'id' => $this->primaryKey(),
                'parentId' => $this->integer(),
                'active' => $this->tinyInteger(1)->notNull()->defaultValue(0),
                'title' => $this->string(64)->notNull(),
                'description' => $this->text(),
                'content' => $this->text(),
                'metaKeys' => $this->string(64),
                'metaDescription' => $this->string(128),
                'alias' => $this->string(64),
                'created_at' => $this->dateTime(),
                'updated_at' => $this->dateTime(),
            ]
        );

        $this->createIndex(
            'idx-categories-parentId',
            'categories',
            'parentId'
        );

        $this->createIndex(
            'idx-categories-active',
            'categories',
            'active'
        );

        $this->createIndex(
            'idx-categories-alias',
            'categories',
            'alias'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-categories-parentId',
            'categories'
        );

        $this->dropIndex(
            'idx-categories-active',
            'categories'
        );

        $this->dropIndex(
            'idx-categories-alias',
            'categories'
        );

        $this->dropTable('categories');
    }
}
