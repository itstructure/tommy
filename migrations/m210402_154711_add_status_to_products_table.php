<?php

use yii\db\Migration;

/**
 * Class m210402_154711_add_status_to_products_table
 */
class m210402_154711_add_status_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'status', $this->tinyInteger(1)->notNull()->defaultValue(0));

        $this->createIndex(
            'idx-products-status',
            'products',
            'status'
        );

        $this->execute('UPDATE products SET status=isNovelty');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-products-status',
            'products'
        );

        $this->dropColumn('products', 'status');
    }
}
