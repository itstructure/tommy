<?php

use yii\db\Migration;

/**
 * Class m210404_152320_add_worktime_to_contacts_table
 */
class m210404_152320_add_worktime_to_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contacts', 'workTime', $this->string(64));
        $this->addColumn('contacts', 'workTime2', $this->string(64));
        $this->addColumn('contacts', 'workTime3', $this->string(64));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contacts', 'workTime');
        $this->dropColumn('contacts', 'workTime2');
        $this->dropColumn('contacts', 'workTime3');
    }
}
