<?php

use yii\db\Migration;

/**
 * Class m201002_102945_add_order_to_pages_table
 */
class m201002_102945_add_order_to_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pages', 'order', $this->tinyInteger());

        $this->createIndex(
            'idx-pages-order',
            'pages',
            'order'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-pages-order',
            'pages'
        );

        $this->dropColumn('pages', 'order');
    }
}
