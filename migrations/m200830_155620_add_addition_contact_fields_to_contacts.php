<?php

use yii\db\Migration;

/**
 * Class m200830_155620_add_addition_contact_fields_to_contacts
 */
class m200830_155620_add_addition_contact_fields_to_contacts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contacts', 'mapApiKey', $this->string(128));
        $this->addColumn('contacts', 'address2', $this->string(128));
        $this->addColumn('contacts', 'address3', $this->string(128));
        $this->addColumn('contacts', 'email2', $this->string(64));
        $this->addColumn('contacts', 'email3', $this->string(64));
        $this->addColumn('contacts', 'phone2', $this->string(32));
        $this->addColumn('contacts', 'phone3', $this->string(32));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contacts', 'mapApiKey');
        $this->dropColumn('contacts', 'address2');
        $this->dropColumn('contacts', 'address3');
        $this->dropColumn('contacts', 'email2');
        $this->dropColumn('contacts', 'email3');
        $this->dropColumn('contacts', 'phone2');
        $this->dropColumn('contacts', 'phone3');
    }
}
