<?php

use yii\db\Migration;

/**
 * Class m201002_084327_add_position_to_pages_table
 */
class m201002_084327_add_position_to_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pages', 'position', $this->tinyInteger());

        $this->createIndex(
            'idx-pages-position',
            'pages',
            'position'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-pages-position',
            'pages'
        );

        $this->dropColumn('pages', 'position');
    }
}
