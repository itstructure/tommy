<?php

use yii\db\Migration;

/**
 * Class m210402_155503_remove_is_novelty_from_products_table
 */
class m210402_155503_remove_is_novelty_from_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex(
            'idx-products-isNovelty',
            'products'
        );

        $this->dropColumn('products', 'isNovelty');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('products', 'isNovelty', $this->tinyInteger(1)->notNull()->defaultValue(0));

        $this->createIndex(
            'idx-products-isNovelty',
            'products',
            'isNovelty'
        );

        $this->execute('UPDATE products SET isNovelty=status');
    }
}
