<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sell_settings}}`.
 */
class m210409_160943_create_sell_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sell_settings', [
            'deliveryConditionText' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sell_settings');
    }
}
