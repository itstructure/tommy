<?php

use yii\db\Migration;

/**
 * Class m200806_175422_change_string_sizes_in_mediafiles_table
 */
class m200806_175422_change_string_sizes_in_mediafiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('mediafiles', 'alt', $this->string(128));
        $this->alterColumn('mediafiles', 'title', $this->string(128));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('mediafiles', 'alt', $this->string(64));
        $this->alterColumn('mediafiles', 'title', $this->string(64));
    }
}
