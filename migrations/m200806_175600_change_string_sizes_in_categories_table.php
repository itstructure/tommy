<?php

use yii\db\Migration;

/**
 * Class m200806_175600_change_string_sizes_in_categories_table
 */
class m200806_175600_change_string_sizes_in_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('categories', 'title', $this->string(128)->notNull());
        $this->alterColumn('categories', 'alias', $this->string(128));
        $this->alterColumn('categories', 'metaKeys', $this->string(128));
        $this->alterColumn('categories', 'metaDescription', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('categories', 'title', $this->string(64)->notNull());
        $this->alterColumn('categories', 'alias', $this->string(64));
        $this->alterColumn('categories', 'metaKeys', $this->string(64));
        $this->alterColumn('categories', 'metaDescription', $this->string(128));
    }
}
