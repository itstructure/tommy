<?php

namespace app\helpers;

use Yii;
use yii\helpers\Html;
use app\models\{Contact, Page};

/**
 * Class BaseHelper
 *
 * @package app\helpers
 */
class BaseHelper
{
    /**
     * @param string $date
     * @param string $format
     *
     * @return string
     */
    public static function getDateAt(string $date, string $format = 'd.m.Y H:i:s'): string
    {
        $inDateTime = new \DateTime($date);

        return date($format, $inDateTime->getTimestamp());
    }

    /**
     * @return string
     */
    public static function ip_address()
    {
        $fields = array('HTTP_CLIENT_IP', 'HTTP_X_REAL_IP', 'HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR');

        $ip = "0.0.0.0";
        foreach($fields as $k)
        {
            if(!empty($_SERVER[$k]) && ip2long($_SERVER[$k]) != false)
            {
                $ip = $_SERVER[$k];
            }
        }

        return $ip;
    }

    /**
     * @return mixed|string
     */
    public static function getCorporateEmail()
    {
        $contacts = Contact::getDefaultContacts();

        if (empty($contacts) || empty($contacts->email)) {
            return !empty(Yii::$app->params['adminEmail']) ? Yii::$app->params['adminEmail'] : '';
        }

        return $contacts->email;
    }

    /**
     * @param $s
     * @return mixed|string
     */
    public static function translit($s)
    {
        $s = (string) $s;
        $s = strip_tags($s);
        $s = str_replace(array("\n", "\r"), " ", $s);
        $s = preg_replace("/\s+/", ' ', $s);
        $s = trim($s);
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
        $s = str_replace(" ", "-", $s);

        return $s;
    }

    /**
     * @param string|null $position
     * @param string|null $title
     * @return string
     */
    public static function getPositionImage(string $position = null, string $title = null): string
    {
        if ($position == Page::POSITION_TOP_MENU) {
            $src = '@web/img/position_top_menu.png';

        } else if ($position == Page::POSITION_FOOTER_1) {
            $src = '@web/img/position_footer_1.png';

        } else if ($position == Page::POSITION_FOOTER_2) {
            $src = '@web/img/position_footer_2.png';

        } else {
            $src = '@web/img/position_neutral.png';
        }

        return Html::img($src, [
            'title' => empty($title) ? static::getPositionTitle($position) : $title
        ]);
    }

    /**
     * @param string|null $position
     * @return mixed
     */
    public static function getPositionTitle(string $position = null)
    {
        $positions = Page::getPositions();

        if ($position == Page::POSITION_NEUTRAL || $position == null) {
            return $positions[Page::POSITION_NEUTRAL];

        } else {
            return $positions[$position];
        }
    }

    /**
     * @param array|\yii\db\ActiveRecord[] $pages
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function groupByPosition(array $pages): array
    {
        $entries = [];

        foreach ($pages as $page) {
            $entries[$page->position][] = $page;
        }

        return $entries;
    }
}
