<?php

namespace app\models\sitemap;

use yii\helpers\Url;
use Itstructure\Sitemap\interfaces\Basic;

/**
 * Class SitemapNovelty
 *
 * @package app\commands\models\sitemap
 */
class SitemapNovelty implements Basic
{
    /**
     * @inheritdoc
     */
    public function getSitemapItems($lang = null)
    {
        return [
            [
                'loc' => Url::to('/novelties', true),
                'lastmod' => $this->getSitemapLastmod(),
                'changefreq' => $this->getSitemapChangefreq(),
                'priority' => $this->getSitemapPriority(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSitemapItemsQuery($lang = null)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getSitemapLoc($lang = null)
    {
        return Url::to('/novelties', true);
    }

    /**
     * @inheritdoc
     */
    public function getSitemapLastmod($lang = null)
    {
        return (new \DateTime())->getTimestamp();
    }

    /**
     * @inheritdoc
     */
    public function getSitemapChangefreq($lang = null)
    {
        return static::CHANGEFREQ_MONTHLY;
    }

    /**
     * @inheritdoc
     */
    public function getSitemapPriority($lang = null)
    {
        return static::PRIORITY_8;
    }
}
