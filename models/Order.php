<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * @package app\models
 */
class Order extends Model
{
    const DELIVERY_SELF = 'self';
    const DELIVERY_POST = 'post';

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var array
     */
    public $quantity = [];

    /**
     * @var string
     */
    public $delivery;

    /**
     * @var string
     */
    public $address;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'email',
                    'quantity',
                    'delivery',
                ],
                'required'
            ],
            [
                'delivery',
                'string',
                'max' => 4
            ],
            [
                'delivery',
                function($attribute){
                    if ($this->{$attribute} != self::DELIVERY_POST) {
                        return;
                    }
                    if (empty(trim($this->address))) {
                        $this->addError('address', Yii::t('order', 'Must fill delivery address'));
                    }
                    if (empty(trim($this->phone))) {
                        $this->addError('phone', Yii::t('order', 'Must fill phone'));
                    }
                },
            ],
            [
                'quantity',
                'each',
                'rule' => ['integer'],
            ],
            [
                [
                    'name',
                    'email',
                    'phone',
                ],
                'string',
                'max' => 64
            ],
            [
                [
                    'comment',
                    'address',
                ],
                'string',
                'max' => 2048
            ],
            [
                'email',
                'email'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('order', 'Name'),
            'email' => Yii::t('order', 'Email'),
            'phone' => Yii::t('order', 'Phone'),
            'comment' => Yii::t('order', 'Comment'),
            'address' => Yii::t('order', 'Address'),
        ];
    }

    /**
     * @param $email
     * @return bool
     */
    public function handle($email)
    {
        if (!$this->validate()) {
            return false;
        }

        $products = Product::find()->where(['in', 'id', array_keys($this->quantity)])->select(['id', 'alias', 'title', 'price'])->all();

        return Yii::$app->mailer->compose('order', [
            'header' => Yii::t('order', 'New order'),
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'comment' => $this->comment,
            'counts' => $this->quantity,
            'products' => $products,
            'baseUrl' => Yii::$app->homeUrl,
            'delivery_method' => $this->getDeliveryMethod(),
            'address' => $this->delivery == self::DELIVERY_POST ? $this->address : null
        ])
            ->setTo($email)
            ->setBcc(Yii::$app->params['bccAdminEmail'])
            ->setFrom([$this->email => $this->name])
            ->setSubject(Yii::t('order', 'New order in {project_name} site.', ['project_name' => Yii::$app->params['project_name']]))
            ->send();
    }

    public function saveOrderItem(): void
    {
        $orderItem = new OrderItem();
        $orderItem->setAttributes($this->getAttributes(), false);
        $orderItem->save();
    }

    /**
     * @return null|string
     */
    private function getDeliveryMethod()
    {
        switch ($this->delivery) {
            case self::DELIVERY_SELF:
                return Yii::t('order', 'Get by yourself');
                break;
            case self::DELIVERY_POST:
                return Yii::t('order', 'Receive by post');
                break;
            default:
                return null;
            break;
        }
    }
}
