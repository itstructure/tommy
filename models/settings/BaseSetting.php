<?php

namespace app\models\settings;

use yii\base\Model;
use yii\db\Connection;

/**
 * Class BaseSetting
 * @package app\models\settings
 */
abstract class BaseSetting extends Model
{
    /**
     * Data base connection driver.
     *
     * @var Connection
     */
    private $db;

    /**
     * @return string
     */
    abstract protected function getTableName(): string;

    /**
     * Set db driver.
     *
     * @param Connection $db
     */
    public function setDb(Connection $db): void
    {
        $this->db = $db;
    }

    /**
     * Returns this object with field's values.
     *
     * @return $this
     */
    public function getSettings()
    {
        $result = $this->db->createCommand('SELECT * FROM ' . $this->getTableName())
            ->queryOne();

        $this->setAttributes($result, false);

        return $this;
    }

    /**
     * Save settings data.
     *
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $count = (int)$this->db->createCommand('SELECT COUNT(*) FROM ' . $this->getTableName())->queryScalar();

        if ($count > 0) {
            $result = $this->db->createCommand('UPDATE ' . $this->getTableName(). ' SET ' . $this->queryUpdateAttributes())
                ->bindValues($this->attributesToBind())
                ->execute();
        } else {
            $result = $this->db->createCommand('INSERT INTO ' . $this->getTableName(). ' ' . $this->queryInsertAttributes())
                ->bindValues($this->attributesToBind())
                ->execute();
        }

        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    private function attributesToBind(): array
    {
        $attributesToBind = [];

        foreach ($this->getAttributes() as $name => $value) {
            $attributesToBind[':' . $name] = $value;
        }

        return $attributesToBind;
    }

    /**
     * @return string
     */
    private function queryUpdateAttributes(): string
    {
        $toUpdateArray = [];

        foreach ($this->getAttributes() as $name => $value) {
            $toUpdateArray[] = $name . ' = :' . $name;
        }

        return implode(', ', $toUpdateArray);
    }

    /**
     * @return string
     */
    private function queryInsertAttributes(): string
    {
        $toInsertArrayNames = [];
        $toInsertArrayValues = [];

        foreach ($this->getAttributes() as $name => $value) {
            $toInsertArrayNames[] = $name;
            $toInsertArrayValues[] = ':' . $name;
        }

        return
            ' (' . implode(', ', $toInsertArrayNames) . ') VALUES ' .
            ' (' . implode(', ', $toInsertArrayValues) . ') ';
    }
}
