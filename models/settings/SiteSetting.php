<?php

namespace app\models\settings;

/**
 * This is the model class for table "settings".
 * @package app\models\settings
 */
class SiteSetting extends BaseSetting
{
    /**
     * @var int
     */
    public $initUserStatus;

    /**
     * @var string
     */
    public $initUserRole;

    /**
     * @var int
     */
    public $regBlock;

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'initUserStatus',
                    'initUserRole',
                    'regBlock',
                ],
                'required',
            ],
            [
                [
                    'initUserStatus',
                    'regBlock',
                ],
                'integer',
            ],
            [
                [
                    'initUserRole',
                ],
                'string',
                'max' => 64
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'initUserStatus' => 'User status after registration',
            'initUserRole' => 'User role after registration',
            'regBlock' => 'Block registration',
        ];
    }
}
