<?php

namespace app\models\settings;

use Yii;

/**
 * This is the model class for table "sell_settings".
 * @package app\models\settings
 */
class SellSetting extends BaseSetting
{
    /**
     * @var string
     */
    public $deliveryConditionText;

    /**
     * @return string
     */
    protected function getTableName(): string
    {
        return 'sell_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'deliveryConditionText',
                ],
                'string',
                'max' => 255
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'deliveryConditionText' => Yii::t('order', 'Delivery condition text'),
        ];
    }
}
