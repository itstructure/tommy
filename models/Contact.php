<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property int $default
 * @property string $title
 * @property string $address
 * @property string $address2
 * @property string $address3
 * @property string $email
 * @property string $email2
 * @property string $email3
 * @property string $phone
 * @property string $phone2
 * @property string $phone3
 * @property string $workTime
 * @property string $workTime2
 * @property string $workTime3
 * @property string $metaKeys
 * @property string $metaDescription
 * @property string $created_at
 * @property string $updated_at
 * @property int $pageId
 * @property string $mapQ
 * @property int $mapZoom
 * @property string $mapApiKey
 *
 * @property ContactSocial[] $contactSocial
 * @property Social[] $social
 * @property Page $page
 *
 * @package app\models
 */
class Contact extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'title'
                ],
                'required'
            ],
            [
                [
                    'title',
                    'metaKeys',
                    'metaDescription'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'address',
                    'address2',
                    'address3',
                ],
                'string',
                'max' => 128
            ],
            [
                [
                    'email',
                    'email2',
                    'email3',
                    'workTime',
                    'workTime2',
                    'workTime3',
                ],
                'string',
                'max' => 64
            ],
            [
                [
                    'phone',
                    'phone2',
                    'phone3',
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'default',
                    'mapZoom',
                    'pageId'
                ],
                'integer'
            ],
            [
                'mapQ',
                'string',
                'max' => 255
            ],
            [
                'mapApiKey',
                'string',
                'max' => 128
            ],
            [
                'title',
                'unique',
                'skipOnError'     => true,
                'targetClass'     => static::class,
                'filter' => $this->getScenario() == self::SCENARIO_UPDATE ? 'id != '.$this->id : ''
            ],
            [
                [
                    'pageId'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Page::class,
                'targetAttribute' => ['pageId' => 'id']
            ],
            [
                [
                    'created_at',
                    'updated_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'default' => Yii::t('app', 'Default'),
            'title' => Yii::t('contacts', 'Title'),
            'address' => Yii::t('contacts', 'Address'),
            'address2' => Yii::t('contacts', 'Address 2'),
            'address3' => Yii::t('contacts', 'Address 3'),
            'email' => Yii::t('contacts', 'Email'),
            'email2' => Yii::t('contacts', 'Email 2'),
            'email3' => Yii::t('contacts', 'Email 3'),
            'phone' => Yii::t('contacts', 'Phone'),
            'phone2' => Yii::t('contacts', 'Phone 2'),
            'phone3' => Yii::t('contacts', 'Phone 3'),
            'workTime' => Yii::t('contacts', 'Work time'),
            'workTime2' => Yii::t('contacts', 'Work time 2'),
            'workTime3' => Yii::t('contacts', 'Work time 3'),
            'metaKeys' => Yii::t('app', 'Meta keys'),
            'metaDescription' => Yii::t('app', 'Meta description'),
            'created_at' => Yii::t('app', 'Created date'),
            'updated_at' => Yii::t('app', 'Updated date'),
            'mapQ' => Yii::t('contacts', 'Map place'),
            'mapZoom' => Yii::t('contacts', 'Map zoom'),
            'mapApiKey' => Yii::t('contacts', 'Map Api Key'),
            'pageId' => Yii::t('contacts', 'Link with page'),
        ];
    }

    /**
     * Returns the default contacts record.
     *
     * @return null|\yii\db\ActiveRecord
     */
    public static function getDefaultContacts()
    {
        return static::find()
            ->where([
                'default' => 1
            ])
            ->one();
    }

    /**
     * Reset the default contacts record.
     *
     * @param boolean $insert
     *
     * @return mixed
     */
    public function beforeSave($insert)
    {
        if ($this->default == 1) {

            $default = static::findOne([
                'default' => 1,
            ]);

            if (null !== $default) {
                $default->default = 0;
                $default->save();
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactSocial()
    {
        return $this->hasMany(ContactSocial::class, [
            'contacts_id' => 'id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocial()
    {
        return $this->hasMany(Social::class, [
            'id' => 'social_id'
        ])->viaTable('contacts_social', [
            'contacts_id' => 'id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::class, [
            'id' => 'pageId'
        ]);
    }
}
