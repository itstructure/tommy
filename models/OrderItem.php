<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_items".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $delivery
 * @property string $comment
 * @property string $address
 * @property array $quantity
 * @property string $created_at
 * @property string $updated_at
 *
 * @package app\models
 */
class OrderItem extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'email',
                    'phone',
                ],
                'string',
                'max' => 64
            ],
            [
                'delivery',
                'string',
                'max' => 4
            ],
            [
                [
                    'comment',
                    'address',
                ],
                'string',
                'max' => 2048
            ],
            [
                'quantity',
                'filter',
                'filter' => function ($value) {
                    return empty($value) ? '' : serialize($value);
                }
            ],
            [
                [
                    'created_at',
                    'updated_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('order', 'Name'),
            'email' => Yii::t('order', 'Email'),
            'phone' => Yii::t('order', 'Phone'),
            'comment' => Yii::t('order', 'Comment'),
            'delivery' => Yii::t('order', 'Delivery method'),
            'address' => Yii::t('order', 'Delivery address'),
            'quantity' => Yii::t('order', 'Quantity'),
        ];
    }

    /**
     * @return array|mixed
     */
    public function getQuantity()
    {
        if (empty($this->quantity)) {
            return [];
        }
        return unserialize($this->quantity);
    }
}
