<?php

namespace app\models;

use Yii;
use Itstructure\MultiLevelMenu\MenuWidget;
use app\helpers\BaseHelper;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $metaKeys
 * @property string $metaDescription
 * @property string $created_at
 * @property string $updated_at
 * @property int $parentId
 * @property string $icon
 * @property string $alias
 * @property int $active
 * @property int $position
 * @property int $order
 * @property int|null $maxOrder
 * @property int|null $minOrder
 * @property array|\yii\db\ActiveRecord[] $contacts
 *
 * @package app\models
 */
class Page extends ActiveRecord
{
    const POSITION_NEUTRAL = 0;
    const POSITION_TOP_MENU = 1;
    const POSITION_FOOTER_1 = 2;
    const POSITION_FOOTER_2 = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'active',
                    'alias',
                ],
                'required',
            ],
            [
                [
                    'description',
                    'content',
                ],
                'string',
            ],
            [
                [
                    'title',
                    'metaKeys',
                    'metaDescription',
                    'alias',
                ],
                'string',
                'max' => 255,
            ],
            [
                [
                    'parentId',
                    'active',
                    'position',
                    'order'
                ],
                'integer',
            ],
            [
                'icon',
                'string',
                'max' => 64,
            ],
            [
                'parentId',
                'filter',
                'filter' => function ($value) {
                    if (empty($value)) {
                        return null;
                    } else {
                        return MenuWidget::checkNewParentId($this, $value) ? $value : $this->getOldAttribute('parentId');
                    }
                }
            ],
            [
                'alias',
                'filter',
                'filter' => function ($value) {
                    return BaseHelper::translit($value);
                }
            ],
            [
                'alias',
                'unique',
                'skipOnError'     => true,
                'targetClass'     => static::class,
                'filter' => $this->getScenario() == self::SCENARIO_UPDATE ? 'id != '.$this->id : ''
            ],
            [
                'title',
                'unique',
                'skipOnError'     => true,
                'targetClass'     => static::class,
                'filter' => $this->getScenario() == self::SCENARIO_UPDATE ? 'id != '.$this->id : ''
            ],
            [
                [
                    'created_at',
                    'updated_at',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        return [
            'id',
            'parentId',
            'icon',
            'alias',
            'active',
            'title',
            'description',
            'content',
            'metaKeys',
            'metaDescription',
            'created_at',
            'updated_at',
            'position',
            'order',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parentId' => Yii::t('app', 'Parent object'),
            'icon' => Yii::t('app', 'Icon'),
            'active' => Yii::t('app', 'Active status'),
            'alias' => Yii::t('app', 'URL Alias'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'content' => Yii::t('app', 'Content'),
            'metaKeys' => Yii::t('app', 'Meta keys'),
            'metaDescription' => Yii::t('app', 'Meta description'),
            'created_at' => Yii::t('app', 'Created date'),
            'updated_at' => Yii::t('app', 'Updated date'),
            'position' => Yii::t('pages', 'Position'),
            'order' => Yii::t('pages', 'Order'),
        ];
    }

    /**
     * Reassigning child objects to their new parent after delete the main model record.
     */
    public function afterDelete()
    {
        MenuWidget::afterDeleteMainModel($this);

        \Yii::$app->db->createCommand("UPDATE pages SET `order` = `order`-1 WHERE `order` > :current_order")
            ->bindValue(':current_order', $this->order)
            ->execute();

        parent::afterDelete();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord){
            $this->order = is_null($this->maxOrder) ? 1 : $this->maxOrder + 1;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return int|null
     */
    public function getMinOrder()
    {
        $result = static::find()->select('order')->orderBy('order ASC')->one();

        return is_null($result) ? 1 : $result->order;
    }

    /**
     * @return int|null
     */
    public function getMaxOrder()
    {
        $result = static::find()->select('order')->orderBy('order DESC')->one();

        return is_null($result) ? 1 : $result->order;
    }

    /**
     * @param int $order
     */
    public function setOrderValue(int $order): void
    {
        if ($order == $this->order){
            return;
        }

        /** @var Page $future */
        $future = static::find()
            ->where([
                'order' => $order
            ])
            ->one();
        $future->order = $order > $this->order ? $order - 1 : $order + 1;
        $future->save();

        $this->order = $order;
        $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::class, [
            'pageId' => 'id'
        ])->where(['contacts.default' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getPagesQuery()
    {
        return static::find()->select([
            'id', 'parentId', 'title', 'alias', 'position'
        ]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllPages()
    {
        return static::getPagesQuery()->orderBy([
            'order' => SORT_ASC
        ])->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllActivePages()
    {
        return static::getPagesQuery()->where([
            'active' => 1
        ])->orderBy([
            'order' => SORT_ASC
        ])->all();
    }

    /**
     * @return array
     */
    public static function getPositions()
    {
        return [
            self::POSITION_NEUTRAL => Yii::t('pages', 'Neutral'),
            self::POSITION_TOP_MENU => Yii::t('pages', 'Top menu'),
            self::POSITION_FOOTER_1 => Yii::t('pages', 'Footer 1'),
            self::POSITION_FOOTER_2 => Yii::t('pages', 'Footer 2'),
        ];
    }
}
