<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $header string */
/* @var $name string */
/* @var $email string */
/* @var $phone string */
/* @var $comment string */
/* @var $delivery_method string */
/* @var $address string */
/* @var $baseUrl string */
/* @var $counts array */
/* @var $products \app\models\Product[] */
?>

<h2><?php echo $header; ?>:</h2>

<p><?php echo Yii::t('order', 'Name'); ?>: <?php echo $name; ?></p>

<?php if (!empty($email)): ?>
    <p><?php echo Yii::t('order', 'Email'); ?>: <?php echo $email; ?></p>
<?php endif; ?>

<?php if (!empty($phone)): ?>
    <p><?php echo Yii::t('order', 'Phone'); ?>: <?php echo $phone; ?></p>
<?php endif; ?>

<?php if (!empty($comment)): ?>
    <p><?php echo Yii::t('order', 'Comment'); ?>: <?php echo $comment; ?></p>
<?php endif; ?>

<?php if (!empty($delivery_method)): ?>
    <p><?php echo Yii::t('order', 'Delivery method'); ?>: <?php echo $delivery_method; ?></p>
<?php endif; ?>

<?php if (!empty($address)): ?>
    <p><?php echo Yii::t('order', 'Delivery address'); ?>: <?php echo $address; ?></p>
<?php endif; ?>

<table class="table">
    <thead>
        <tr>
            <td><?php echo Yii::t('order', 'Good') ?></td>
            <td><?php echo Yii::t('products', 'Price') ?>, <?php echo Yii::t('products', 'rub.') ?></td>
            <td><?php echo Yii::t('order', 'Quantity') ?></td>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product) { ?>
        <tr>
            <td>
                <p><?php echo Html::a($product->title, Url::to(rtrim($baseUrl, '/').'/product/'.$product->alias), ['target' => '_blank']); ?></p>
            </td>
            <td>
                <p><?php echo $product->price; ?></p>
            </td>
            <td>
                <p><?php echo $counts[$product->id]; ?></p>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
