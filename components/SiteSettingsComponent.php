<?php

namespace app\components;

use Yii;
use yii\base\{Model, Component};
use app\models\settings\SiteSetting;

/**
 * Class SiteSettingsComponent
 * Component class for settings tuning.
 *
 * @package app\components
 */
class SiteSettingsComponent extends Component
{
    /**
     * Sets Settings model.
     *
     * @return Model
     */
    public function setModel(): Model
    {
        /** @var Model $object */
        $object = Yii::createObject([
            'class' => SiteSetting::class,
            'db' => Yii::$app->db
        ]);

        return $object;
    }
}
