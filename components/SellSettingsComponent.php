<?php

namespace app\components;

use Yii;
use yii\base\{Model, Component};
use app\models\settings\SellSetting;

/**
 * Class SellSettingsComponent
 * Component class for settings tuning.
 *
 * @package app\components
 */
class SellSettingsComponent extends Component
{
    /**
     * Sets Settings model.
     *
     * @return Model
     */
    public function setModel(): Model
    {
        /** @var Model $object */
        $object = Yii::createObject([
            'class' => SellSetting::class,
            'db' => Yii::$app->db
        ]);

        return $object;
    }
}
