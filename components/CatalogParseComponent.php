<?php

namespace app\components;

use Yii;
use yii\imagine\Image;
use yii\base\Component;
use yii\db\ActiveRecord;
use yii\helpers\{BaseFileHelper, BaseConsole};
use Itstructure\MFUploader\interfaces\{UploadComponentInterface, UploadModelInterface};
use Itstructure\MFUploader\models\Mediafile;
use app\components\classes\CatalogTmpFile;
use app\helpers\BaseHelper;

/**
 * Class CatalogParseComponent
 *
 * @package app\components
 */
class CatalogParseComponent extends Component
{
    const LEVEL_COLORS = [
        BaseConsole::FG_BLUE,
        BaseConsole::FG_YELLOW,
        BaseConsole::FG_GREY,
        BaseConsole::FG_CYAN,
    ];

    /**
     * @var string
     */
    public $sourceDir;

    /**
     * @var string
     */
    public $folder;

    /**
     * @var bool
     */
    public $consoleLogging = false;

    /**
     * @var string
     */
    public $uploadComponentName;

    /**
     * @var array
     */
    public $allowedFileTypes = [UploadModelInterface::FILE_TYPE_IMAGE];

    /**
     * @var string
     */
    public $uploadSubDir = 'products';

    /**
     * @var bool
     */
    public $initImageResize = true;

    /**
     * @var int
     */
    public $maxImageWidth = 970;

    /**
     * @var string
     */
    public $parentModelParentKey = 'parentId';

    /**
     * @var string
     */
    public $parentModelNameAttribute = 'title';

    /**
     * @var string
     */
    public $parentModelAliasAttribute = 'alias';

    /**
     * @var array
     */
    public $parentModelAdditionAttributes = [];

    /**
     * @var string
     */
    public $childModelParentKey = 'categoryId';

    /**
     * @var string
     */
    public $childModelNameAttribute = 'title';

    /**
     * @var string
     */
    public $childModelAliasAttribute = 'alias';

    /**
     * @var array
     */
    public $childModelAdditionAttributes = [];

    /**
     * @var string|ActiveRecord
     */
    private $parentModelClass;

    /**
     * @var string|ActiveRecord
     */
    private $childModelClass;

    /**
     * @var UploadComponentInterface
     */
    private $fileUploader;

    /**
     * Init
     */
    public function init()
    {
        if (!empty($this->uploadComponentName)) {
            $this->fileUploader = Yii::$app->get($this->uploadComponentName);
        }
    }

    /**
     * @param string $parentModelClass
     */
    public function setParentModelClass(string $parentModelClass): void
    {
        $this->checkModelClass($parentModelClass);
        $this->parentModelClass = $parentModelClass;
    }

    /**
     * @param string $childModelClass
     */
    public function setChildModelClass(string $childModelClass): void
    {
        $this->checkModelClass($childModelClass);
        $this->childModelClass = $childModelClass;
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        if (empty($this->sourceDir)) {
            throw new \Exception('Param sourceDir must be set.');
        }

        if (empty($this->folder)) {
            $sourcePath = rtrim(rtrim($this->sourceDir, '/'), '\\');

        } else {
            $sourcePath = rtrim(rtrim($this->sourceDir, '/'), '\\') . DIRECTORY_SEPARATOR . trim(trim($this->folder, '/'), '\\');
        }

        $this->scanCatalog($sourcePath);
    }

    /**
     * @param string $path
     */
    private function scanCatalog(string $path)
    {
        $directories = BaseFileHelper::findDirectories($path, ['recursive' => false]);

        $this->scanDirectories($directories, 0);
    }

    /**
     * @param array $directories
     * @param int $level
     * @param int|null $parentId
     */
    private function scanDirectories(array $directories, int $level, int $parentId = null)
    {
        foreach ($directories as $directoryPath) {

            $files = BaseFileHelper::findFiles($directoryPath, ['recursive' => false]);

            $directories = BaseFileHelper::findDirectories($directoryPath, ['recursive' => false]);

            $categoryName = basename($directoryPath);

            if (empty($files) && empty($directories)) {
                if ($this->consoleLogging) {
                    echo $this->consoleLog(($level == 0 ? "Category '" : "Subcategory '") . $categoryName. "' is empty." . "\n", $level, [BaseConsole::FG_RED]);
                }
                continue;
            }

            $parentModelClass = $this->parentModelClass;

            $parentModel = $parentModelClass::find()->where([$this->parentModelNameAttribute => $categoryName])->one();

            if (empty($parentModel)) {
                if ($this->consoleLogging) {
                    echo $this->consoleLog(($level == 0 ? "Create category: '" : "Create subcategory: '") . $categoryName ."'". "\n", $level, [$this->getColorCode($level)]);
                }

                /* @var ActiveRecord $parentModel */
                $parentModel = new $parentModelClass();
                $parentModel->{$this->parentModelNameAttribute} = $categoryName;
                if (!empty($files)) {
                    $parentModel->{$this->parentModelAliasAttribute} = BaseHelper::translit($categoryName);
                }
                if (!empty($parentId)) {
                    $parentModel->{$this->parentModelParentKey} = $parentId;
                }
                $this->setAdditionAttributes($parentModel, $this->parentModelAdditionAttributes)->save();

            } else if ($this->consoleLogging) {
                echo $this->consoleLog(($level == 0 ? "Category '" : "Subcategory '") . $categoryName. "' already exists." . "\n", $level, [BaseConsole::FG_RED]);
            }

            $this->scanFiles($files, $level + 1, $parentModel);

            $this->scanDirectories($directories, $level + 1, $parentModel->getPrimaryKey());
        }
    }

    /**
     * @param array $files
     * @param int $level
     * @param ActiveRecord $parentModel
     */
    private function scanFiles(array $files, int $level, ActiveRecord $parentModel)
    {
        foreach ($files as $filePath) {
            $fileInfo = pathinfo($filePath);
            $fileMimeType = BaseFileHelper::getMimeType($filePath);

            if (!$this->checkAllowedType($fileMimeType)) {
                if ($this->consoleLogging) {
                    echo $this->consoleLog("File MIME Type '" . $fileMimeType. "' of '".$fileInfo['basename']."' is not allowed!" . "\n", $level, [BaseConsole::FG_RED]);
                }
                continue;
            }

            $childEntityName = $fileInfo['filename'];

            $childModelClass = $this->childModelClass;

            $childModel = $childModelClass::find()->where([$this->childModelNameAttribute => $childEntityName])->one();

            if (empty($childModel)) {
                if ($this->consoleLogging) {
                    echo $this->consoleLog("Create product: '" . $childEntityName ."'". "\n", $level, [$this->getColorCode($level)]);
                }

                /* @var ActiveRecord $childModel */
                $childModel = new $childModelClass();
                $childModel->{$this->childModelNameAttribute} = $childEntityName;
                $childModel->{$this->childModelAliasAttribute} = BaseHelper::translit($childEntityName);
                $childModel->{$this->childModelParentKey} = $parentModel->getPrimaryKey();
                $this->setAdditionAttributes($childModel, $this->childModelAdditionAttributes)->save();

                if ($this->consoleLogging) {
                    echo $this->consoleLog("Send file: " . $fileInfo['basename']. "\n", $level, [$this->getColorCode($level)]);
                }
                $this->sendFile($filePath, $fileMimeType, $fileInfo['basename'], $childModel);

            } else if ($this->consoleLogging) {
                echo $this->consoleLog("Product '" . $childEntityName. "' already exists." . "\n", $level, [BaseConsole::FG_RED]);
            }
        }
    }

    /**
     * @param string $filePath
     * @param string $fileMimeType
     * @param string $fileName
     * @param ActiveRecord $childModel
     */
    private function sendFile(string $filePath, string $fileMimeType, string $fileName, ActiveRecord $childModel): void
    {
        $shortFileType = $this->getShortFileType($fileMimeType);

        if ($this->initImageResize && $shortFileType == UploadModelInterface::FILE_TYPE_IMAGE) {
            $this->imageResize($filePath);
        }

        $tmpFileObj = new CatalogTmpFile([
            'name' => $fileName,
            'tempName' => $filePath,
            'type' => $fileMimeType,
            'size' => filesize($filePath),
            'error' => UPLOAD_ERR_OK,
        ]);
        $uploadModel = $this->getUploadModel();
        $uploadModel->setAttributes([
            'subDir' => $this->uploadSubDir,
            'title' => $fileName,
            'alt' => $fileName,
            'owner' => $childModel::tableName(),
            'ownerId' => $childModel->getPrimaryKey(),
            'ownerAttribute' => $shortFileType == UploadModelInterface::FILE_TYPE_IMAGE ? UploadModelInterface::FILE_TYPE_THUMB : $shortFileType
        ], false);
        $uploadModel->setFile($tmpFileObj);
        $uploadModel->save();

        if ($shortFileType == UploadModelInterface::FILE_TYPE_IMAGE) {
            $uploadModel->createThumbs();
        }
    }

    /**
     * @param ActiveRecord $model
     * @param array $attributes
     * @return ActiveRecord
     */
    private function setAdditionAttributes(ActiveRecord $model, array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $model->{$key} = $value;
        }
        return $model;
    }

    /**
     * @return UploadModelInterface
     */
    private function getUploadModel(): UploadModelInterface
    {
        return $this->fileUploader->setModelForSave(new Mediafile());
    }

    /**
     * @param string $fileMimeType
     * @return bool
     */
    private function checkAllowedType(string $fileMimeType): bool
    {
        $allow = false;

        foreach ($this->allowedFileTypes as $type) {
            if (strpos($this->getShortFileType($fileMimeType), $type) !== false) {
                $allow = true;
                break;
            }
        }

        return $allow;
    }

    /**
     * @param string $fileMimeType
     * @return string
     */
    private function getShortFileType(string $fileMimeType): string
    {
        if (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_IMAGE) !== false) {
            return UploadModelInterface::FILE_TYPE_IMAGE;

        } elseif (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_AUDIO) !== false) {
            return UploadModelInterface::FILE_TYPE_AUDIO;

        } elseif (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_VIDEO) !== false) {
            return UploadModelInterface::FILE_TYPE_VIDEO;

        } elseif (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_APP) !== false) {
            return UploadModelInterface::FILE_TYPE_APP;

        } elseif (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_TEXT) !== false) {
            return UploadModelInterface::FILE_TYPE_TEXT;

        } elseif (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_APP_WORD) !== false) {
            return UploadModelInterface::FILE_TYPE_APP_WORD;

        } elseif (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_APP_EXCEL) !== false) {
            return UploadModelInterface::FILE_TYPE_APP_EXCEL;

        } elseif (strpos($fileMimeType, UploadModelInterface::FILE_TYPE_APP_PDF) !== false) {
            return UploadModelInterface::FILE_TYPE_APP_PDF;

        } else {
            return UploadModelInterface::FILE_TYPE_OTHER;
        }
    }

    /**
     * @param string $filePath
     */
    private function imageResize(string $filePath): void
    {
        Image::$driver = [Image::DRIVER_GD2, Image::DRIVER_GMAGICK, Image::DRIVER_IMAGICK];

        $imageObj = Image::getImagine()->open($filePath);

        $sizeBox = $imageObj->getSize();
        $width = $sizeBox->getWidth();
        $height = $sizeBox->getHeight();

        if ($width > $this->maxImageWidth) {
            $diff = round(($width/$height), 2);
            $resultWidth = $this->maxImageWidth;
            $resultHeight = (int)(floor($resultWidth/$diff));
            Image::resize($imageObj, $resultWidth, $resultHeight, false)->save($filePath);
        }
    }

    /**
     * @param int $level
     * @return int|mixed
     */
    private function getColorCode(int $level)
    {
        return !empty(self::LEVEL_COLORS[$level]) ? self::LEVEL_COLORS[$level] : BaseConsole::FG_PURPLE;
    }

    /**
     * @param string $message
     * @param int|null $level
     * @param array $format
     * @return string
     */
    private function consoleLog(string $message, int $level = null, array $format = [BaseConsole::FG_BLUE])
    {
        $space = '';

        for ($i=0; $i < $level; $i++) {
            $space .= '  ';
        }

        if ($level > 0) {
            $space .= "'-";
        }

        return BaseConsole::ansiFormat($space . $message, $format);
    }

    /**
     * @param string $modelClass
     * @throws \Exception
     */
    private function checkModelClass(string $modelClass)
    {
        $modelClassParents = class_parents($modelClass);

        if (!isset($modelClassParents[ActiveRecord::class])) {
            throw new \Exception('Model class '.$modelClass.' must be extended from "'.ActiveRecord::class.'".');
        }
    }
}
