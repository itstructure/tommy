<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\BaseConsole;
use Itstructure\MFUploader\components\LocalUploadComponent;
use Itstructure\MFUploader\interfaces\{UploadComponentInterface, UploadModelInterface};
use Itstructure\MFUploader\models\Mediafile;

/**
 * Class ImageRebuildComponent
 *
 * @package app\components
 */
class ImageRebuildComponent extends Component
{
    const LEVEL_COLORS = [
        BaseConsole::FG_BLUE,
        BaseConsole::FG_YELLOW,
        BaseConsole::FG_GREY,
        BaseConsole::FG_CYAN,
    ];

    /**
     * @var bool
     */
    public $consoleLogging = false;

    /**
     * @var string
     */
    public $uploadComponentName;

    /**
     * @var array
     */
    public $allowedFileTypes = [UploadModelInterface::FILE_TYPE_IMAGE];

    /**
     * @var UploadComponentInterface|LocalUploadComponent
     */
    private $fileUploader;

    /**
     * Init
     */
    public function init()
    {
        if (!empty($this->uploadComponentName)) {
            $this->fileUploader = Yii::$app->get($this->uploadComponentName);
        }
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $this->scanImages();
    }

    private function scanImages()
    {
        $entries = Mediafile::find()->all();

        foreach ($entries as $entry) {
            /** @var Mediafile $entry */
            $level = 0;

            $originalFilePath = $this->fileUploader->uploadRoot . DIRECTORY_SEPARATOR . ltrim(ltrim($entry->url, '\\'), '/');

            if (!is_readable($originalFilePath)) {
                if ($this->consoleLogging) {
                    echo $this->consoleLog("Mediafile ID " . ($entry->id) . ". Original file " . $originalFilePath . " does not exist or not readable" . "\n", $level, [BaseConsole::FG_RED]);
                }
                continue;

            } else if (!$this->checkAllowedType($entry->type)) {
                if ($this->consoleLogging) {
                    echo $this->consoleLog("Mediafile ID " . ($entry->id) . ". File MIME Type '" . $entry->type. "' of '".$originalFilePath."' is not allowed!" . "\n", $level, [BaseConsole::FG_RED]);
                }
                continue;

            } else if ($this->consoleLogging) {
                echo $this->consoleLog("Mediafile ID ".($entry->id).". Work with original file: " . $originalFilePath . "\n", $level, [$this->getColorCode($level)]);
            }

            $oldThumbs = $entry->getThumbs();

            foreach ($oldThumbs as $thumb) {
                $oldThumbPath = $this->fileUploader->uploadRoot . DIRECTORY_SEPARATOR . ltrim(ltrim($thumb, '\\'), '/');
                $oldThumbPath = str_replace('\\', DIRECTORY_SEPARATOR, $oldThumbPath);
                if (!unlink($oldThumbPath)) {
                    if ($this->consoleLogging) {
                        echo $this->consoleLog("Error delete thumb file: " . $oldThumbPath . "\n", $level+1, [BaseConsole::FG_RED]);
                    }

                } else if ($this->consoleLogging) {
                    echo $this->consoleLog("Deleted thumb file: " . $oldThumbPath . "\n", $level+1, [$this->getColorCode($level+1)]);
                }
            }

            $uploadModel = $this->fileUploader->setModelForSave($entry);

            if (!$uploadModel->createThumbs()) {
                if ($this->consoleLogging) {
                    echo $this->consoleLog("Error create new thumbs" . "\n", $level+1, [BaseConsole::FG_RED]);
                }
            } else if ($this->consoleLogging) {
                foreach ($uploadModel->getMediafileModel()->getThumbs() as $newThumb) {
                    $newThumbPath = $this->fileUploader->uploadRoot . DIRECTORY_SEPARATOR . ltrim(ltrim($newThumb, '\\'), '/');
                    echo $this->consoleLog("Created new thumb file: " . $newThumbPath . "\n", $level+1, [$this->getColorCode($level+2)]);
                }
            }
        }
    }

    /**
     * @param int $level
     * @return int|mixed
     */
    private function getColorCode(int $level)
    {
        return !empty(self::LEVEL_COLORS[$level]) ? self::LEVEL_COLORS[$level] : BaseConsole::FG_PURPLE;
    }

    /**
     * @param string $message
     * @param int|null $level
     * @param array $format
     * @return string
     */
    private function consoleLog(string $message, int $level = null, array $format = [BaseConsole::FG_BLUE])
    {
        $space = '';

        for ($i=0; $i < $level; $i++) {
            $space .= '  ';
        }

        if ($level > 0) {
            $space .= "'-";
        }

        return BaseConsole::ansiFormat($space . $message, $format);
    }

    /**
     * @param string $fileMimeType
     * @return bool
     */
    private function checkAllowedType(string $fileMimeType): bool
    {
        $allow = false;

        foreach ($this->allowedFileTypes as $type) {
            if (strpos($fileMimeType, $type) !== false) {
                $allow = true;
                break;
            }
        }

        return $allow;
    }
}
