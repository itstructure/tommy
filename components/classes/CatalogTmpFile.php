<?php
namespace app\components\classes;

use yii\web\UploadedFile;

/**
 * Class CatalogTmpFile
 * @package app\components\classes
 */
class CatalogTmpFile extends UploadedFile
{
    /**
     * @param string $file
     * @param bool $deleteTempFile
     * @return bool
     */
    public function saveAs($file, $deleteTempFile = false)
    {
        if ($this->error == UPLOAD_ERR_OK) {
            if ($deleteTempFile) {
                return move_uploaded_file($this->tempName, $file);
            } else {
                return copy($this->tempName, $file);
            }
        }

        return false;
    }
}