### CHANGE LOG:

**2.1.0 July 18, 2020:**
- Ability to use `environment` config file.
- Readme fixes.

**2.0.2 July 18, 2020:**
- Set requirement for php>=7.1.0.
- Upgrades for: RBAC, MFU and MultiLevel-menu modules.

**2.0.1 July 17, 2020:**
- Readme fixes.

**2.0.0 July 16, 2020:**
- Set config for Base Url.

**1.0.0 July 15, 2020:**
- First base installations.