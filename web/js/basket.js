/**
 * @param id
 * @param count
 */
function putToBasket(id, count) {
    var url = "/ajax/order-ajax/put-to-basket";
    var params = {
        _csrf: window.yii.getCsrfToken(),
        id: id,
        count: count !== undefined ? count : 1
    };
    AJAX(url, 'POST', params, {
        response_json: true,
        func_callback: function (resp) {
            if (resp.meta.status == 'success') {
                $('[role="total_amount"]').html(resp.data.total_amount);
            }
        },
        func_error: function (resp, xhr) {
            console.error(resp.message);
        }
    });
}

/**
 * @param id
 * @param count
 */
function setCountInBasket(id, count) {
    var url = "/ajax/order-ajax/set-count-in-basket";
    var params = {
        _csrf: window.yii.getCsrfToken(),
        id: id,
        count: count
    };
    AJAX(url, 'POST', params, {
        response_json: true,
        func_callback: function (resp) {
            if (resp.meta.status == 'success') {
                $('[role="total_amount"]').html(resp.data.total_amount);
                $('#item_price_'+id).html(resp.data.item_price);
                $('#total_price_'+id).html(parseInt(count)*parseFloat(resp.data.item_price));
            }
        },
        func_error: function (resp, xhr) {
            console.error(resp.message);
        }
    });
}

/**
 * @param id
 */
function removeFromBasket(id) {
    var url = "/ajax/order-ajax/remove-from-basket";
    var params = {
        _csrf: window.yii.getCsrfToken(),
        id: id
    };
    AJAX(url, 'POST', params, {
        response_json: true,
        func_callback: function (resp) {
            if (resp.meta.status == 'success') {
                $('#row_order_'+id).remove();
                $('[role="total_amount"]').html(resp.data.total_amount);
                if (resp.data.total_count == 0) {
                    var send_order_el = $('#send_order_block');
                    if (!send_order_el.hasClass('hidden')) {
                        send_order_el.addClass('hidden');
                    }
                }
            }
        },
        func_error: function (resp, xhr) {
            console.error(resp.message);
        }
    });
}
