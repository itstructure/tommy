$(document).ready(function() {

    var sendOrderForm = $('#main_order_form');
    sendOrderForm.on("click", '[role="send"]', function(e) {
        e.preventDefault();
        if (recaptcha_status == 'passive' || recaptcha_status == 'unvalid') {
            var textAlert;
            switch (recaptcha_status) {
                case 'passive':
                    textAlert = window.need_captcha;
                    break;
                case 'unvalid':
                    textAlert = window.error_captcha;
                    break;
            }
            showAlert(sendOrderForm, textAlert, 'danger');
            return;
        }
        closeAlert(sendOrderForm);

        var url = '/ajax/order-ajax/send-order',
            nameBlock = sendOrderForm.find('[role="name"]'),
            emailBlock = sendOrderForm.find('[role="email"]'),
            phoneBlock = sendOrderForm.find('[role="phone"]'),
            commentBlock = sendOrderForm.find('[role="comment"]'),
            deliveryBlock = sendOrderForm.find('[role="delivery"]:checked'),
            addressBlock = sendOrderForm.find('[role="address"]'),

            params = {
                _csrf: window.yii.getCsrfToken(),
                name: nameBlock.val(),
                email: emailBlock.val(),
                phone: phoneBlock.val(),
                comment: commentBlock.val(),
                delivery: deliveryBlock.val(),
                address: addressBlock.val()
            };

        var quantityBlocks = sendOrderForm.find('[role="quantity"]');
        quantityBlocks.each(function () {
            params[$(this)[0].name] = $(this)[0].value;
        });

        AJAX(url, 'POST', params, {
            response_json: true,
            func_waiting: function () {
                showPreloader();
            },
            func_callback: function (resp) {
                hidePreloader();
                if (resp.meta.status == 'success') {

                    sendOrderForm.find('.form-group').each(function () {
                        if ($(this).hasClass('has-error')) {
                            $(this).removeClass('has-error');
                        }
                        $(this).find('.help-block').text('');
                        $(this).find('.form-control').val('');
                    });

                    $('#cart_items').remove();
                    $('#send_order_block').remove();
                    $('[role="total_amount"]').html(0);
                    showAlert(sendOrderForm, resp.meta.message, 'success');

                } else if (resp.meta.status == 'fail') {
                    var errors = resp.data.errors;
                    sendOrderForm.find('.form-group').each(function () {
                        var dataGroup = $(this).attr('data-group');
                        var help_block = $('#help_block_'+dataGroup);
                        if (dataGroup in errors) {
                            if (!$(this).hasClass('has-error')) {
                                $(this).addClass('has-error');
                            }
                            help_block.text(errors[dataGroup][0]);
                        } else {
                            if ($(this).hasClass('has-error')) {
                                $(this).removeClass('has-error');
                            }
                            help_block.text('');
                        }
                    });
                }
            },
            func_error: function (resp) {
                console.log(resp.message);
            }
        });
    });
});

var validateRecaptchaOrder = function () {
    validateRecaptcha({
        func_waiting: function () {

        },
        func_callback_error: function () {
            recaptcha_status = 'unvalid';
        },
        func_callback_success: function () {
            recaptcha_status = 'valid';
        }
    });
};

document.addEventListener("DOMContentLoaded", function () {
    var delivery_tabs_el = $('#delivery_tabs');
    delivery_tabs_el.on('click', 'a[role=tab] input[type=radio]', function(event) {
        event.stopPropagation();
        $(this).parent().tab('show');
    });
    delivery_tabs_el.on('show.bs.tab', 'a[role=tab]', function() {
        $('input[type=radio]').removeAttr('checked');
        $(this).find('input[type=radio]').prop('checked', true);
    });
});
