<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * FontElegantAsset bundle.
 */
class EshopperAsset extends AssetBundle
{
    public $sourcePath = '@webroot/eshopper';
    public $css = [
        'css/bootstrap.min.css',
        'css/fontawesome-pro-5.13.0-web.min.css',
        'css/prettyPhoto.css',
        'css/price-range.css',
        'css/animate.css',
        'css/main.css',
        'css/responsive.css',
    ];
    public $js = [
        'js/jquery.js',
        'js/bootstrap.min.js',
        'js/jquery.scrollUp.min.js',
        'js/price-range.js',
        'js/jquery.prettyPhoto.js',
        'js/main.js',

        /*[if lt IE 9]*/
        'js/html5shiv.js',
        //'js/respond.min.js'
        /* [endif] */
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
