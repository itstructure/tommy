<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class OrderAsset
 *
 * @package app\assets
 */
class OrderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/preloader.css',
    ];
    public $js = [
        'js/preloader.js',
        'js/order.js',
        'https://www.google.com/recaptcha/api.js',
    ];
    public $depends = [
        AppAsset::class,
    ];
}
