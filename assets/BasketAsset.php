<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class BasketAsset
 *
 * @package app\assets
 */
class BasketAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        'js/basket.js',
    ];
    public $depends = [
        AppAsset::class,
    ];
}
