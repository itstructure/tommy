<?php

namespace app\controllers;

use Itstructure\MFUploader\models\album\Album;
use Itstructure\MFUploader\models\Mediafile;
use Itstructure\MFUploader\models\OwnerMediafile;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\filters\{AccessControl, VerbFilter};
use app\models\{Home, Product};
use yii\helpers\FileHelper;

/**
 * Class HomeController
 *
 * @package app\controllers
 */
class HomeController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                ],
            ]
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = Home::getDefaultHome();

        $this->setMetaParams($model);

        $productsQuery = Product::find()->where([
            'active' => 1
        ])->where([
            'in', 'status', [
                Product::STATUS_NOVELTY, Product::STATUS_REGULAR
            ]
        ])->orderBy('status DESC, title ASC');

        $pagination = new Pagination([
            'totalCount' => $productsQuery->count(),
            'defaultPageSize' => Yii::$app->params['defaultPageSize']
        ]);

        $activeQuery = OwnerMediafile::find();
        $activeQuery->where(['owner' => 'otherAlbum']);
        $ownerMediaFileOtherAlbums = $activeQuery->all();

        return $this->render('index', [
            'model' => $model, // why?
            'ownerBanners' => $ownerMediaFileOtherAlbums,
            'pagination' => $pagination,
            'products' => $productsQuery->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all()
        ]);
    }
}
