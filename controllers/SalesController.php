<?php

namespace app\controllers;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\filters\{AccessControl, VerbFilter};
use app\models\Product;

/**
 * Class SalesController
 *
 * @package app\controllers
 */
class SalesController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'view' => ['get'],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionView()
    {
        $this->view->title = 'Распродажа';

        $this->setMetaKeys('распродажа, подарки, tommy, gifts');
        $this->setMetaDescription('Распродажа подарков томми, распродажа сувениров о томске');

        $noveltyQuery = Product::find()->where([
            'status' => Product::STATUS_SALE
        ])->andWhere([
            'active' => 1
        ])->orderBy('title ASC');

        $pagination = new Pagination([
            'totalCount' => $noveltyQuery->count(),
            'defaultPageSize' => Yii::$app->params['defaultPageSize']
        ]);

        return $this->render('view', [
            'pagination' => $pagination,
            'products' => $noveltyQuery->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all()
        ]);
    }
}
