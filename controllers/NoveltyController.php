<?php

namespace app\controllers;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\filters\{AccessControl, VerbFilter};
use app\models\Product;

/**
 * Class NoveltyController
 *
 * @package app\controllers
 */
class NoveltyController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'view' => ['get'],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionView()
    {
        $this->view->title = 'Новинки';

        $this->setMetaKeys('новинки, подарки, tommy, gifts');
        $this->setMetaDescription('Новые подарки томми, новые сувениры о томске');

        $noveltyQuery = Product::find()->where([
            'status' => Product::STATUS_NOVELTY
        ])->andWhere([
            'active' => 1
        ])->orderBy('title ASC');

        $pagination = new Pagination([
            'totalCount' => $noveltyQuery->count(),
            'defaultPageSize' => Yii::$app->params['defaultPageSize']
        ]);

        return $this->render('view', [
            'pagination' => $pagination,
            'products' => $noveltyQuery->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all()
        ]);
    }
}
