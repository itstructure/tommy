<?php

namespace app\controllers;

use Yii;
use yii\db\ActiveRecord;
use yii\web\Controller;
use app\models\{Category, Contact, Page, Product};
use app\traits\BasketTrait;
use app\helpers\BaseHelper;

/**
 * Class BaseController
 *
 * @package app\controllers
 */
class BaseController extends Controller
{
    use BasketTrait;

    /**
     * @var string
     */
    public $layout = '@app/views/layouts/base';

    /**
     * @var Contact
     */
    protected $contacts;

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $this->initBasket();

        $this->contacts = Contact::getDefaultContacts();
        $this->view->params['pages'] = BaseHelper::groupByPosition(Page::getAllActivePages());
        $this->view->params['categories'] = Category::getActiveMenu();
        $this->view->params['contacts'] = $this->contacts;
        $this->view->params['controllerId'] = Yii::$app->controller->id;
        $this->view->params['total_amount'] = $this->totalAmount;
        $this->view->params['socials'] = [];

        if ($this->contacts && $this->contacts->social) {
            $this->view->params['socials'] = $this->contacts->social;
        }

        $this->view->params['noveltiesExist'] = Product::find()->where([
            'status' => Product::STATUS_NOVELTY
        ])->andWhere([
            'active' => 1
        ])->count() > 0;

        $this->view->params['salesExist'] = Product::find()->where([
            'status' => Product::STATUS_SALE
        ])->andWhere([
            'active' => 1
        ])->count() > 0;

        return true;
    }

    /**
     * @param ActiveRecord|null $model
     */
    protected function setMetaParams(ActiveRecord $model = null)
    {
        if (null === $model) {
            return;
        }

        $this->view->title = $model->title;

        if (!empty($model->metaKeys)) {
            $this->setMetaKeys($model->metaKeys);
        }

        if (!empty($model->metaDescription)) {
            $this->setMetaDescription($model->metaDescription);
        }

        $this->view->registerLinkTag([
            'rel' => 'canonical',
            'href' => rtrim(Yii::$app->request->absoluteUrl, '/')
        ]);

        $this->view->registerLinkTag([
            'rel' => 'alternate',
            'hreflang' => 'x-default',
            'href' => Yii::$app->request->hostInfo
        ]);
    }

    /**
     * @param string $metaKeys
     */
    protected function setMetaKeys(string $metaKeys): void
    {
        $this->setMetaAttribute('keywords', $metaKeys);
    }

    /**
     * @param string $metaDescription
     */
    protected function setMetaDescription(string $metaDescription): void
    {
        $this->setMetaAttribute('description', $metaDescription);
    }

    /**
     * @param string $name
     * @param string $content
     */
    private function setMetaAttribute(string $name, string $content): void
    {
        $this->view->registerMetaTag([
            'name' => $name,
            'content' => $content
        ]);
    }
}
