<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\filters\{AccessControl, VerbFilter};

/**
 * Class CartController
 *
 * @package app\controllers
 */
class CartController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                ],
            ],
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionIndex()
    {
        if ($this->basketProducts) {
            $view = 'index';
            $params = [
                'products' => $this->basketProducts,
                'counts' => $this->basketCounts,
                'sellSettings' => Yii::$app->get('sell-settings')
                    ->setModel()
                    ->getSettings()
            ];

        } else {
            $view = 'empty';
            $params = [];
        }

        return $this->render($view, $params);
    }
}
