<?php

namespace app\controllers\admin;

use Yii;
use app\models\{Page, PageSearch};
use app\traits\{AdminBeforeActionTrait, AccessTrait};
use Itstructure\AdminModule\controllers\CommonAdminController;

/**
 * Class PageController
 * PageController implements the CRUD actions for Page model.
 *
 * @package app\controllers\admin
 */
class PageController extends CommonAdminController
{
    use AdminBeforeActionTrait, AccessTrait;

    /**
     * @var bool
     */
    protected $setEditingScenarios = true;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!$this->checkAccessToIndex()) {
            return $this->accessError();
        }

        $request = Yii::$app->request;

        if ($request->get('id') != null && $request->get('order') != null) {

            /** @var Page $pageModel */
            $pageModel = $this->findModel($request->get('id'));
            $pageModel->setOrderValue($request->get('order'));

            return $this->redirect([$this->urlPrefix.'index']);
        }

        return parent::actionIndex();
    }

    /**
     * @param int|string $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        if (!$this->checkAccessToView()) {
            return $this->accessError();
        }

        return parent::actionView($id);
    }

    /**
     * @return mixed|string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (!$this->checkAccessToCreate()) {
            return $this->accessError();
        }

        $this->additionFields['pages'] = Page::getAllPages();

        return parent::actionCreate();
    }

    /**
     * @param int|string $id
     *
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        if (!$this->checkAccessToUpdate()) {
            return $this->accessError();
        }

        $this->additionFields['pages'] = Page::getAllPages();

        return parent::actionUpdate($id);
    }

    /**
     * @param int|string $id
     *
     * @return mixed|\yii\web\Response
     */
    public function actionDelete($id)
    {
        if (!$this->checkAccessToDelete()) {
            return $this->accessError();
        }

        return parent::actionDelete($id);
    }

    /**
     * Returns Page model name.
     *
     * @return string
     */
    protected function getModelName():string
    {
        return Page::class;
    }

    /**
     * Returns PageSearch model name.
     *
     * @return string
     */
    protected function getSearchModelName():string
    {
        return PageSearch::class;
    }
}
