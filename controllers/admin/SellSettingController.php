<?php

namespace app\controllers\admin;

use Yii;
use app\traits\{AdminBeforeActionTrait, AccessTrait};
use Itstructure\AdminModule\controllers\AdminController;

/**
 * Class SellSettingController
 * SellSettingController implements the CRUD actions for SellSetting model.
 *
 * @package app\controllers\admin
 */
class SellSettingController extends AdminController
{
    use AdminBeforeActionTrait, AccessTrait;

    /**
     * List of records.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!$this->checkAccessToUpdate()) {
            return $this->accessError();
        }

        /* @var $model \app\models\settings\SellSetting */
        $model = Yii::$app->get('sell-settings')
            ->setModel()
            ->getSettings();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                '/admin/sell-settings'
            ]);
        }

        $fields = [
            'model' => $model,
        ];

        return $this->render('index', $fields);
    }
}
