<?php

namespace app\controllers\admin;

use Yii;
use app\traits\{AdminBeforeActionTrait, AccessTrait};
use Itstructure\AdminModule\controllers\AdminController;

/**
 * Class SiteSettingController
 * SiteSettingController implements the CRUD actions for SiteSetting model.
 *
 * @package app\controllers\admin
 */
class SiteSettingController extends AdminController
{
    use AdminBeforeActionTrait, AccessTrait;

    /**
     * List of records.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!$this->checkAccessToAdministrate()) {
            return $this->accessError();
        }

        /* @var $model \app\models\settings\SiteSetting */
        $model = Yii::$app->get('site-settings')
            ->setModel()
            ->getSettings();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                '/admin/site-settings'
            ]);
        }

        $fields = [
            'model' => $model,
            'roles' => Yii::$app->authManager->getRoles()
        ];

        return $this->render('index', $fields);
    }
}
