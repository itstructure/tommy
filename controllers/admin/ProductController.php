<?php

namespace app\controllers\admin;

use Yii;
use app\models\{Category, Product, ProductSearch};
use app\traits\{AdminBeforeActionTrait, AccessTrait, AdditionFieldsTrait};
use Itstructure\MFUploader\interfaces\UploadModelInterface;
use Itstructure\AdminModule\controllers\CommonAdminController;
use Itstructure\MFUploader\traits\MediaFilesTrait;
use Itstructure\MFUploader\Module as MFUploader;

/**
 * Class ProductController
 * ProductController implements the CRUD actions for Product model.
 *
 * @package app\controllers\admin
 */
class ProductController extends CommonAdminController
{
    use AdminBeforeActionTrait, AccessTrait, AdditionFieldsTrait, MediaFilesTrait;

    /**
     * @var bool
     */
    protected $setEditingScenarios = true;

    /**
     * @var bool
     */
    protected $enableAdditionImages = true;

    /**
     * @var bool
     */
    protected $enableAdditionAlbums = false;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!$this->checkAccessToIndex()) {
            return $this->accessError();
        }

        return parent::actionIndex();
    }

    /**
     * @param int|string $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        if (!$this->checkAccessToView()) {
            return $this->accessError();
        }

        $this->additionFields['enableAdditionAlbums'] = $this->enableAdditionAlbums;
        $this->additionFields['enableAdditionImages'] = $this->enableAdditionImages;
        $this->additionFields['images'] = $this->enableAdditionImages ? $this->getMediaFiles(Product::tableName(), (int)$id, UploadModelInterface::FILE_TYPE_IMAGE) : [];

        return parent::actionView($id);
    }

    /**
     * @return mixed|string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (!$this->checkAccessToCreate()) {
            return $this->accessError();
        }

        $this->additionFields['categories'] = Category::getMenu();
        $this->additionFields['enableAdditionAlbums'] = $this->enableAdditionAlbums;
        $this->additionFields['albums'] = $this->enableAdditionAlbums ? $this->getAlbums() : [];

        return parent::actionCreate();
    }

    /**
     * @param int|string $id
     *
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        if (!$this->checkAccessToUpdate()) {
            return $this->accessError();
        }

        if (Yii::$app->request->isPost) {
            try {
                $delete_mediafiles_ids = Yii::$app->request->post('delete_mediafiles');
                if (!empty($delete_mediafiles_ids)) {
                    $this->deleteMediafileEntry($delete_mediafiles_ids, Yii::$app->getModule(MFUploader::MODULE_NAME));
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage());
            }
        }

        $this->additionFields['categories'] = Category::getMenu();
        $this->additionFields['enableAdditionAlbums'] = $this->enableAdditionAlbums;
        $this->additionFields['enableAdditionImages'] = $this->enableAdditionImages;
        $this->additionFields['albums'] = $this->enableAdditionAlbums ? $this->getAlbums() : [];
        $this->additionFields['images'] = $this->enableAdditionImages ? $this->getMediaFiles(Product::tableName(), (int)$id, UploadModelInterface::FILE_TYPE_IMAGE) : [];

        return parent::actionUpdate($id);
    }

    /**
     * @param int|string $id
     *
     * @return mixed|\yii\web\Response
     */
    public function actionDelete($id)
    {
        if (!$this->checkAccessToDelete()) {
            return $this->accessError();
        }

        try {
            $this->deleteMediafiles(Product::tableName(), $id, Yii::$app->getModule(MFUploader::MODULE_NAME));
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
        }

        return parent::actionDelete($id);
    }

    /**
     * Returns Product model name.
     *
     * @return string
     */
    protected function getModelName():string
    {
        return Product::class;
    }

    /**
     * Returns ProductSearch model name.
     *
     * @return string
     */
    protected function getSearchModelName():string
    {
        return ProductSearch::class;
    }
}
