<?php

namespace app\traits;

use Yii;
use app\components\BasketComponent;
use app\models\Product;

/**
 * Class BasketTrait
 * @package app\traits
 */
trait BasketTrait
{
    /**
     * @var BasketComponent
     */
    protected $basketManager;

    /**
     * @var array
     */
    protected $basketCounts = [];

    /**
     * @var Product[]
     */
    protected $basketProducts = [];

    /**
     * @var int
     */
    protected $totalAmount = 0;

    /**
     * Initialize.
     */
    public function init()
    {
        parent::init();

        $this->basketManager = Yii::$app->get('basket');
    }

    /**
     * Init orders for a basket.
     */
    protected function initBasket()
    {
        $this->basketCounts = $this->basketManager->retrieveSessionData();
        $this->basketProducts = $this->basketManager->getModelItems();
        $this->totalAmount = $this->basketManager->calculateTotalAmount($this->basketProducts);
    }
}