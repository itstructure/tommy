<?php

namespace app\commands;

use Yii;
use yii\helpers\{ArrayHelper, BaseConsole};
use yii\console\{Controller, ExitCode};
use app\components\CatalogParseComponent;

/**
 * Class CatalogParseController
 *
 * @package app\commands
 */
class CatalogParseController extends Controller
{
    /**
     * @var string
     */
    public $folder;

    /**
     * @var CatalogParseComponent
     */
    private $parser;

    /**
     * Init
     */
    public function init()
    {
        $this->parser = Yii::$app->get('catalogParse');

        $this->color = true;

        parent::init();
    }

    /**
     * @param string $actionID
     * @return array
     */
    public function options($actionID)
    {
        return ArrayHelper::merge(parent::options($actionID), ['folder']);
    }

    /**
     * @return array
     */
    public function optionAliases()
    {
        return ArrayHelper::merge(parent::optionAliases(), ['f' => 'folder']);
    }

    /**
     * @return int
     */
    public function actionIndex()
    {
        try {
            if (!empty($this->folder)) {
                $this->parser->folder = $this->folder;
            }

            $this->parser->run();

            echo $this->ansiFormat("Done\n", BaseConsole::FG_GREEN);
            return ExitCode::OK;

        } catch (\Exception $e) {
            echo $this->ansiFormat("ERROR: ".$e->getMessage()." \n", BaseConsole::FG_RED);
            return ExitCode::UNSPECIFIED_ERROR;
        }
    }
}
