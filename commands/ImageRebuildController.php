<?php

namespace app\commands;

use Yii;
use yii\helpers\BaseConsole;
use yii\console\{Controller, ExitCode};
use app\components\ImageRebuildComponent;

/**
 * Class ImageRebuildController
 *
 * @package app\commands
 */
class ImageRebuildController extends Controller
{
    /**
     * @var ImageRebuildComponent
     */
    private $parser;

    /**
     * Init
     */
    public function init()
    {
        $this->parser = Yii::$app->get('imageRebuild');

        $this->color = true;

        parent::init();
    }

    /**
     * @return int
     */
    public function actionIndex()
    {
        try {
            $this->parser->run();

            echo $this->ansiFormat("Done\n", BaseConsole::FG_GREEN);
            return ExitCode::OK;

        } catch (\Exception $e) {
            echo $this->ansiFormat("ERROR: ".$e->getMessage()." \n", BaseConsole::FG_RED);
            return ExitCode::UNSPECIFIED_ERROR;
        }
    }
}
