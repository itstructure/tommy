<?php
use app\models\Contact;

/* @var Contact $model */

//$this->params['breadcrumbs'][] = $model->title;
?>

<section class="">

    <div id="map" class="map-wrapper margin-bottom"></div>

    <div class="container">

        <div class="row">
            <div class="col-lg-12" data-animated="fadeIn">
                <p class="margin-bottom"><?php echo $model->title ?></p>
                <ul class="margin-bottom-large">
                    <?php if (!empty($model->address)): ?>
                        <li><b class="fa fa-home"></b> <span> <?php echo $model->address ?></span></li>
                    <?php endif; ?>
                    <?php if (!empty($model->phone)): ?>
                        <li><b class="fa fa-phone"></b> <span> <?php echo $model->phone ?></span></li>
                    <?php endif; ?>
                    <?php if (!empty($model->email)): ?>
                        <li><b class="fa fa-envelope"></b> <span><a href="mailto:<?php echo $model->email ?>"> <?php echo $model->email ?></a></span></li>
                    <?php endif; ?>

                    <?php if (is_array($model->social)): ?>
                        <?php foreach ($model->social as $social): ?>
                            <li>
                                <b class="<?php echo $social->icon ?>"></b>
                                <span><a href="<?php echo $social->url ?>" target="_blank" ><?php echo $social->url ?></a></span>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var zoom = '<?php echo $model->mapZoom; ?>';
        var baseAddress = '<?php echo trim($model->address); ?>';
        var address2 = '<?php echo trim($model->address2); ?>';
        var address3 = '<?php echo trim($model->address3); ?>';

        ymaps.ready(init);

        function init() {
            var geoCoder = ymaps.geocode(baseAddress);
            geoCoder.then(
                function (res) {
                    var baseCoordinates = getCoordinates(res);

                    var map = new ymaps.Map("map", {
                        center: [baseCoordinates[1], baseCoordinates[0]],
                        zoom: zoom
                    });

                    addGeoObject(map, baseAddress, {title: baseAddress});

                    if (address2 != '' && address2 != null) {
                        addGeoObject(map, address2, {title: address2});
                    }

                    if (address3 != '' && address3 != null) {
                        addGeoObject(map, address3, {title: address3});
                    }
                },
                function (err) {

                }
            );
        }

        function addGeoObject(map, address, options) {
            ymaps.geocode(address).then(
                function (res) {
                    var coordinates = getCoordinates(res);
                    map.geoObjects.add(new ymaps.GeoObject({
                            geometry: {
                                type: "Point",
                                coordinates: [coordinates[1], coordinates[0]]
                            },
                            properties: options ? {
                                iconContent: options.title !== undefined ? options.title : '',
                                hintContent: options.description !== undefined ? options.description : ''
                            } : {}
                        }, {
                            preset: 'islands#blackStretchyIcon',
                            draggable: false
                        })
                    );
                },
                function (err) {

                }
            );
        }

        function getCoordinates(geoCodeResponse) {
            return geoCodeResponse.geoObjects.properties.get('metaDataProperty').GeocoderResponseMetaData.Point.coordinates;
            //return geoCodeResponse.geoObjects.get(0).geometry.getMap().getCenter();
        }
    });
</script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=<?php echo $model->mapApiKey; ?>" type="text/javascript"></script>
