<?php
use yii\helpers\{Html, Url, ArrayHelper};

/* @var app\models\Category $data */
/* @var int $level */
/* @var bool $hasChildren */

$menuItemClass = ($level == 1) ? 'menu-item-level-1' : '';

if ($level == 0) {
    $url = 'javascript:void(0)';
    $options = [];
    $title = '<span class="menu-title menu-item-level-0">' . $data->title . '</span>';
    $class = '';

} else if ($hasChildren && $level == 1) {
    $url = '#category-'.$data->id;
    $options = [
        'data-toggle' => 'collapse',
        'data-parent' => '#accordian',
    ];
    $title = '<span class="badge pull-right"><i class="fa dynamic-symbol"></i></span>' . '<span class="menu-title parent '.$menuItemClass.'">' . $data->title . '</span>';
    $class = !empty($this->params['categoryParentKeys']) && in_array($data->id, $this->params['categoryParentKeys']) ? '' : 'collapsed';

} else {
    $url = empty($data->alias) ? 'javascript:void(0)' : Url::to('/category/'.$data->alias);
    $options = [];
    $title = '<span class="sub-menu-title menu-item-level-n">' . $data->title . '</span>';
    if ($level == 1) {
        $class = !empty($this->params['categoryId']) && $this->params['categoryId'] == $data->id ? 'highlighted' : '';

    } else {
        $class = 'category-link' . (!empty($this->params['categoryId']) && $this->params['categoryId'] == $data->id ? ' highlighted' : '');
    }
}

$title = Html::a($title, $url, ArrayHelper::merge([
    'target' => '_self',
    'class' => $class

], $options)
);

echo $level == 0 ? Html::tag('h4', $title, ['class' => 'menu-head']) : $title;