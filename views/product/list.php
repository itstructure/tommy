<?php
use app\models\Product;
use app\assets\BasketAsset;
use yii\widgets\LinkPager;
use yii\data\Pagination;
use yii\helpers\Html;

/* @var Product[] $products */
/* @var Pagination $pagination */

BasketAsset::register($this);
?>

<style>
    .d-xs-less, .d-sm, .d-md-more {
        display: none;
    }
    @media (max-width: 767px) {
        .d-xs-less {
            display: block;
        }
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .d-sm {
            display: block;
        }
    }
    @media (min-width: 992px) {
        .d-md-more {
            display: block;
        }
    }
</style>

<?php $i = 1; ?>
<?php foreach ($products as $product) { ?>
    <div class="col-sm-6 col-md-3">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="product-info text-center">
                    <a href="/product/<?php echo $product->alias; ?>">
                        <?php echo Html::img($product->getDefaultThumbUrl(), ['alt' => $product->title]) ?>
                    </a>
                    <p class="product-title"><?php echo $product->title; ?></p>
                </div>
                <?php if ($product->status == Product::STATUS_NOVELTY): ?>
                    <!--<div class="hit-flag"></div>-->
                    <div class="novelty-flag"></div>
                <?php elseif ($product->status == Product::STATUS_SALE): ?>
                    <div class="sale-flag"></div>
                <?php endif; ?>
            </div>
            <?php if (!empty($product->price)): ?>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li class="price"><?php echo $product->price; ?> <?php echo Yii::t('products', 'rub.') ?></li>
                        <li>
                            <a href="javascript:void(0)" class="-btn -btn-default add-to-cart" onclick="putToBasket('<?php echo $product->id; ?>')"><?php /* <i class="fa fa-shopping-cart"></i> */ ?>в корзину</a>
                        </li>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="clearfix d-xs-less"></div>
    <?php if (($i - 2) == 0): ?>
        <div class="clearfix d-sm"></div>
    <?php endif; ?>
    <?php if (($i - 4) == 0): ?>
        <div class="clearfix d-sm"></div>
        <div class="clearfix d-md-more"></div>
    <?php endif; ?>
    <?php if ($i == 4) {$i = 1;} else {$i++;} ?>
<?php } ?>

<div class="row">
    <div class="col-sm-12">
        <?php echo LinkPager::widget(['pagination' => $pagination]); ?>
    </div>
</div>
