<?php
use yii\helpers\Html;
use Itstructure\MFUploader\Module as MFUModule;
use Itstructure\MFUploader\models\Mediafile;
use app\models\Product;
use app\assets\BasketAsset;

/* @var Product $model */
/* @var Mediafile[] $images */

BasketAsset::register($this);
?>
<div class="padding-right">
    <div class="-row product-details">
        <div class="col-sm-6">
            <div class="view-product">

                <?php if ($model->status == Product::STATUS_NOVELTY): ?>
                    <div class="novelty-flag"></div>
                <?php elseif ($model->status == Product::STATUS_SALE): ?>
                    <div class="sale-flag"></div>
                <?php endif ?>

                <?php /*if (!empty($thumbnailModel = $model->getThumbnailModel())): */?><!--
                    <?php /*echo Html::img($thumbnailModel->getThumbUrl(MFUModule::THUMB_ALIAS_ORIGINAL), ['alt' => $model->title]) */?>
                --><?php /*endif; */?>

                <?php if (is_array($images) && $thumbnailModel = $model->getThumbnailModel()) {
                    array_unshift($images, $thumbnailModel);
                } ?>

                <?php if (!empty($images)): ?>

                    <div class="gallery items-1 -autoplay" style="margin-bottom: 20px;">
                        <?php foreach ($images as $key => $img): ?>
                            <div id="item-<?php echo $key+1; ?>" class="control-operator"></div>
                            <figure class="item">
                                <?php echo Html::img($img->getThumbUrl(MFUModule::THUMB_ALIAS_MEDIUM), ['alt' => $img->alt]); ?>
                            </figure>
                        <?php endforeach; ?>
                        <?php if (count($images) > 1) { ?>
                            <div class="controls">
                                <?php foreach ($images as $key => $img): ?>
                                    <a href="#item-<?php echo $key+1; ?>" class="control-button"><span class="control-label">•</span></a> &nbsp;
                                <?php endforeach; ?>
                            </div>
                        <?php } ?>
                    </div>


                    <?php /* ?>
                    <div id="similar-product" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php foreach ($images as $key => $img): ?>
                                <div class="item <?php if ($key == 0): ?>active<?php endif; ?>">
                                    <a href="<?php echo $img->getThumbUrl(MFUModule::THUMB_ALIAS_ORIGINAL); ?>" target="_blank">
                                        <img src="<?php echo $img->getThumbUrl(MFUModule::THUMB_ALIAS_LARGE); ?>" alt="<?php echo $img->alt; ?>">
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- Controls -->
                        <a class="left item-control" href="#similar-product" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right item-control" href="#similar-product" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                    <?php */ ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="product-information">
                <h2 class="margin-bottom"><?php echo $model->title; ?></h2>
                <div class="margin-bottom-large"><?php echo $model->content; ?></div>
                <?php if (!empty($model->price)): ?>
                    <div class="order-entity order-entity-price">
                        <span id="item_price_<?php echo $model->id; ?>"><?php echo $model->price; ?></span>
                        <?php echo Yii::t('products', 'rub.') ?>
                    </div>
                    <div class="order-entity">
                        <input type="number" value="1" id="quantity" class="order-entity-quantity" min="1" onchange="setCountInBasket('<?php echo $model->id; ?>', this.value)"> &nbsp;
                    </div>
                    <div class="order-entity">
                        <button type="button" class="add-to-cart" style="margin-top: -1px;" onclick="putToBasket('<?php echo $model->id; ?>', $('#quantity')[0].value)">
                            <?php /* ?><i class="fa fa-shopping-cart"></i><?php */ ?>
                            в корзину
                        </button>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

