<?php
use yii\data\Pagination;
use app\models\Page;

/* @var Page $model */
/* @var Pagination $pagination */

$this->params['breadcrumbs'][] = $model->title;
?>

<?php if (!empty($model->content)): ?>
    <section class="inform_block">

        <div class="container">

            <?php /*if (!empty($model->description)): */?><!--
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-10">
                        <?php /*echo $model->description */?>
                    </div>
                </div>
            --><?php /*endif; */?>

            <div class="row" data-animated="fadeIn">
                <div class="col-lg-12 col-md-12 col-sm-10">
                    <?php echo $model->content ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if (!empty($model->contacts)): ?>
    <?php echo $this->render('../contact/index.php', [
        'model' => $model->contacts[0],
    ]) ?>
<?php endif; ?>
