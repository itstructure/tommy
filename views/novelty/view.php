<?php
use yii\helpers\Html;

/* @var app\models\Product[] $products */
/* @var yii\data\Pagination $pagination */
?>
<div class="-col-sm-10 -padding-right">
    <div class="features-items">
        <h2 class="title"><?php echo Html::encode($this->title); ?></h2>
        <?php echo $this->render('../product/list', ['products' => $products, 'pagination' => $pagination]) ?>
    </div>
</div>
