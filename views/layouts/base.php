<?php

use yii\helpers\{Html, ArrayHelper};
use Itstructure\MultiLevelMenu\MenuWidget;
use app\assets\AppAsset;
use app\models\Page;

/* @var string $content */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo Html::encode($this->title); ?></title>

    <?php echo Html::csrfMetaTags(); ?>

    <link rel="apple-touch-icon" sizes="76x76" href="/eshopper/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/eshopper/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/eshopper/favicon/favicon-16x16.png">
    <link rel="manifest" href="/eshopper/favicon/site.webmanifest">
    <link rel="mask-icon" href="/eshopper/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta name="yandex-verification" content="e2d4ad2ce48da1ec" />
    <meta name="google-site-verification" content="8gN_9iifOkY8Nk_dRO8juNRVIAUhh_HH7k-I8SBTAQg" />

    <?php /* ?>
    <link rel="shortcut icon" href="/eshopper/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/eshopper/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/eshopper/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/eshopper/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/eshopper/images/ico/apple-touch-icon-57-precomposed.png">
    <?php */ ?>

    <?php $this->head() ?>

    <link href="/dist/gallery.theme.css" rel="stylesheet">
    <link href="/dist/gallery.css" rel="stylesheet">
</head>

<body>
<?php $this->beginBody() ?>

<?php echo $this->render('preloader') ?>

<header id="header">

    <div class="header-middle">
        <div class="container">
            <div class="row" >
                <div class="col-md-6 clearfix">
                    <div class="logo pull-left">
                        <a href="/" class="bg"><img class="logo" src="/eshopper/images/logo.png" alt="Логотип" /></a>
                        <span class="logo-text">душевные сувениры и подарки</span>
                    </div>
                </div>
                <div class="col-md-6 clearfix">
                    <div class="shop-menu clearfix pull-right">
                        <ul class="nav navbar-nav">
                            <?php foreach ($this->params['socials'] as $social) { ?>
                                <li><a href="<?php echo $social->url; ?>" class="bg"><i class="<?php echo $social->icon; ?>"></i></a></li>
                            <?php } ?>
                            <li>
                                <a href="/cart" class="cart-link">
                                    <?php /* ?>
                                    <img width="17" height="22" src="/img/bag.svg" alt="Корзина">
                                    <object type="image/svg+xml" data="/img/bag.svg"></object>
                                    <img src="/img/bag.png" alt="Корзина"> &nbsp;
                                    <i class="fal fa-shopping-bag fa-lg"></i>
                                    <i class="fa fa-shopping-cart fa-lg"></i> &nbsp;
                                    <?php */ ?>
                                    <i class="far fa-shopping-bag fa-lg"></i> &nbsp;
                                    <strong><span role="total_amount"><?php echo $this->params['total_amount']; ?></span> <?php echo Yii::t('products', 'rub.') ?></strong>
                                </a>
                            </li>
                            <?php
                            /*
                            <li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                            <li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                            <li><a href=""><i class="fa fa-user"></i> Account</a></li>
                            <li><a href="login.html"><i class="fa fa-lock"></i> Login</a></li>
                            */
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-11">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle navbar-toggle-page-btn" style="color: white; " data-toggle="collapse" data-target=".navbar-collapse">
                            Меню
                            <?php /* ?>
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <?php */ ?>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="/" class="nav-catalog"><i class="fa fa-bars"></i> Каталог</a></li>

                            <?php if (!empty($this->params['pages'][Page::POSITION_TOP_MENU])): ?>
                                <?php foreach ($this->params['pages'][Page::POSITION_TOP_MENU] as $page): ?>
                                    <li><a href="/page/<?php echo $page->alias; ?>"><?php echo $page->title; ?></a></li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="search_box pull-right">
                        <?php /* ?><input type="text" placeholder="Поиск"/><?php */ ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-2">
                <div class="left-sidebar">

                    <div class="navbar-header" style="padding-bottom: 10px; ">
                        <button class="navbar-toggle navbar-toggle-catalog-btn" style="background: green; color: white;" type="button" data-toggle="collapse" data-target=".catalog-collapse">
                            <i class="fa fa-bars"></i> Каталог
                        </button>
                    </div>
                    <div class="catalog-collapse collapse">

                        <?php if ($this->params['noveltiesExist']): ?>
                        <h4 class="novelties"><a href="/novelties">Новинки</a></h4>
                        <?php endif; ?>

                        <?php if ($this->params['salesExist']): ?>
                        <h4 class="sales"><a href="/sales">Распродажа</a></h4>
                        <?php endif; ?>

                        <?php
                        echo MenuWidget::widget([
                            'data' => $this->params['categories'],
                            'itemTemplate' => '@app/views/menu/categoryItem.php',
                            'mainContainerOptions' => function ($level, $item) {
                                $options = [];
                                if ($level == 0) {
                                    $options['class'] = 'sidebar-menu';
                                } else if ($level == 1) {
                                    $options['class'] = 'menu-list-level-1';
                                } else if ($level == 2 && !empty($item)) {
                                    if (!empty($this->params['categoryParentKeys'])) {
                                        $options['class'] = in_array($item['data']->id, $this->params['categoryParentKeys']) ? 'panel-collapse in menu-list-level-2' : 'panel-collapse collapse menu-list-level-2';
                                        $options['style'] = 'height: auto;';
                                    } else {
                                        $options['class'] = 'panel-collapse collapse menu-list-level-2';
                                    }
                                    $options['id'] = 'category-'.$item['data']->id;
                                } else {
                                    $options['class'] = 'menu-list-level-n';
                                }

                                return $options;
                            },
                            'itemTemplateParams' => function ($level, $item) {
                                return [
                                    'level' => $level,
                                    'hasChildren' => !empty($item['items'])
                                ];
                            }
                        ]); ?>

                    </div>

                </div>
            </div>
            <div class="col-sm-9 col-md-9 col-lg-10" id="content-block">
                <?php echo $content; ?>
            </div>

        </div>
    </div>
</section>

<footer id="footer" class="footer">
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <?php $social_col_width = 6; ?>
                <?php if (!empty($this->params['pages'][Page::POSITION_FOOTER_1])): ?>
                    <?php $page_col_width = floor((12 - $social_col_width) / count($this->params['pages'][Page::POSITION_FOOTER_1])); ?>
                    <?php foreach ($this->params['pages'][Page::POSITION_FOOTER_1] as $page): ?>
                        <div class="col-md-<?php echo $page_col_width; ?> footer-link">
                            <a href="/page/<?php echo $page->alias; ?>" class="bg"><?php echo $page->title; ?></a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-md-<?php echo (12 - $social_col_width) ?>"> </div>
                <?php endif; ?>
                <div class="col-md-<?php echo $social_col_width; ?> footer-social-block">
                    <ul class="list-inline">
                        <?php foreach ($this->params['socials'] as $social) { ?>
                            <li><a href="<?php echo $social->url; ?>" class="bg"><i class="<?php echo $social->icon; ?>"></i></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <?php $copyright_col_width = 8; ?>
                <?php if (!empty($this->params['pages'][Page::POSITION_FOOTER_2])): ?>
                    <?php $page_col_width = floor((12 - $copyright_col_width) / count($this->params['pages'][Page::POSITION_FOOTER_2])); ?>
                    <?php foreach ($this->params['pages'][Page::POSITION_FOOTER_2] as $page): ?>
                        <div class="col-md-<?php echo $page_col_width; ?> footer-link">
                            <a href="/page/<?php echo $page->alias; ?>" class="bg" style="font-size: 90%;"><?php echo $page->title; ?></a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-md-<?php echo (12 - $copyright_col_width) ?>"> </div>
                <?php endif; ?>
                <div class="col-md-<?php echo $copyright_col_width; ?> copyright">© 2020 TommyGiftShop</div>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>

<script type="text/javascript">

    $('.add-to-cart').on('click', function(){

        var closest = $(this).closest('.product-image-wrapper');
        if (closest.length === 0) {
            closest = $(this).closest('.product-details');
        }

        var that = closest.find('img');
        var bascket = $(".cart-link");
        var w = that.width();
        that.clone()
            .css({'width' : w,
                'position' : 'absolute',
                'z-index' : '9999',
                top: that.offset().top,
                left:that.offset().left})
            .appendTo("body")
            .animate({opacity: 0.05,
                left: bascket.offset()['left'],
                top: bascket.offset()['top'],
                width: 20}, 1000, function() {
                $(this).remove();
            });
    });
</script>
</body>
</html>
<?php $this->endPage() ?>