<?php
/* @var app\models\Product[] $products */
/* @var yii\data\Pagination $pagination */
/* @var app\models\Category $model */
?>
<div class="-col-sm-10 -padding-right">
    <div class="features-items">
        <h2 class="title"><?php echo $model->title; ?></h2>
        <?php echo $this->render('../product/list', ['products' => $products, 'pagination' => $pagination]) ?>
    </div>
</div>
