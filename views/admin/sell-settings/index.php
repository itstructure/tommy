<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this Itstructure\AdminModule\components\AdminView */
/* @var $model app\models\settings\SellSetting */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('settings', 'Sell settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">

            <?php echo $form->field($model, 'deliveryConditionText')
                ->textarea([
                    'rows' => 5
                ])
                ->label(Yii::t('order', 'Delivery condition text')) ?>

        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app', 'Update'),
            ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
