<?php

/* @var $this Itstructure\AdminModule\components\AdminView */
/* @var $model app\models\Page */
/* @var $pages array|\yii\db\ActiveRecord[] */

$this->title = Yii::t('pages', 'Update page').': ' . $model->title;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('pages', 'Pages'),
    'url' => [
        $this->params['urlPrefix'].'index'
    ]
];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        $this->params['urlPrefix'].'view',
        'id' => $model->id
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="page-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'pages' => $pages,
    ]) ?>

</div>
