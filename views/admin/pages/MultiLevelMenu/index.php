<?php
use yii\helpers\{Url, Html};
use app\helpers\BaseHelper;
use app\models\Page;

/* @var Page $data */
/* @var string $urlPrefix */
?>
<div class="row">
    <div class="col-md-3">
        <?php echo Html::a(
            Html::encode($data->title),
            Url::to([$urlPrefix.'view', 'id' => $data->id])
        ) ?>
    </div>
    <div class="col-md-1">
        <?php echo BaseHelper::getPositionImage($data->position); ?>
    </div>
    <div class="col-md-1">
        <?php echo Html::tag('div',
                $data->order > $data->minOrder ?
                    Html::a(Html::img('@web/img/sort_asc.png', ['alt' => Yii::t('pages', 'Above'), 'title' => Yii::t('pages', 'Above')]),
                        Url::to([$urlPrefix.'index', 'id' => $data->id, 'order' => $data->order - 1])
                    ) : ''
                ,
                [
                    'style' => 'clear:both;'
                ]
            )
            .
            Html::tag('div',
                $data->order < $data->maxOrder ?
                    Html::a(Html::img('@web/img/sort_desc.png', ['alt' => Yii::t('pages', 'Below'), 'title' => Yii::t('pages', 'Below')]),
                        Url::to([$urlPrefix.'index', 'id' => $data->id, 'order' => $data->order + 1])
                    ) : ''
                ,
                [
                    'style' => 'clear:both;'
                ]
            )
            .
            ($data->minOrder == $data->maxOrder ?
                '-' : ''
            ); ?>
    </div>
    <div class="col-md-2">
        <?php echo Yii::t('app', 'Created date').' '.BaseHelper::getDateAt($data->created_at) ?>
    </div>
    <div class="col-md-2">
        <?php echo Yii::t('app', 'Updated date').' '.BaseHelper::getDateAt($data->updated_at) ?>
    </div>
    <div class="col-md-2">
        <?php if ($data->active == 1): ?>
            <i class="fa fa-check-circle text-success"> <?php echo Yii::t('app', 'Active'); ?></i>
        <?php else: ?>
            <i class="fa fa-times text-danger"> <?php echo Yii::t('app', 'Inactive'); ?></i>
        <?php endif; ?>
    </div>
    <div class="col-md-1">
        <?php echo Html::a(
            Html::tag('span', '', [
                'class' => 'glyphicon glyphicon-eye-open',
            ]),
            Url::to([$urlPrefix.'view', 'id' => $data->id]),
            [
                'title' => 'View',
                'aria-label' => 'View',
                'data-pjax' => '0'
            ]
        ) ?>
        <?php echo Html::a(
            Html::tag('span', '', [
                'class' => 'glyphicon glyphicon-pencil',
            ]),
            Url::to([$urlPrefix.'update', 'id' => $data->id]),
            [
                'title' => 'Update',
                'aria-label' => 'Update',
                'data-pjax' => '0'
            ]
        ) ?>
        <?php echo Html::a(
            Html::tag('span', '', [
                'class' => 'glyphicon glyphicon-trash',
            ]),
            Url::to([$urlPrefix.'delete', 'id' => $data->id]),
            [
                'title' => 'Delete',
                'aria-label' => 'Delete',
                'data-pjax' => '0',
                'data-confirm' => 'Are you sure you want to delete this item?',
                'data-method' => 'post'
            ]
        ) ?>
    </div>
</div>
