<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use Itstructure\MultiLevelMenu\MenuWidget;
use Itstructure\FieldWidgets\{Fields, FieldType};

/* @var $this Itstructure\AdminModule\components\AdminView */
/* @var $model app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
/* @var $pages array|\yii\db\ActiveRecord[] */
?>

<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">

            <?php echo Fields::widget([
                'fields' => [
                    [
                        'name' => 'title',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Title'),
                    ],

                    /* --1-- */
                    [
                        'name' => 'address',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Address'),
                    ],
                    [
                        'name' => 'email',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Email'),
                    ],
                    [
                        'name' => 'phone',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Phone'),
                    ],
                    [
                        'name' => 'workTime',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Work time'),
                    ],

                    /* --2-- */
                    [
                        'name' => 'address2',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Address 2'),
                    ],
                    /*[
                        'name' => 'email2',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Email 2'),
                    ],
                    [
                        'name' => 'phone2',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Phone 2'),
                    ],*/
                    [
                        'name' => 'workTime2',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Work time 2'),
                    ],

                    /* --3-- */
                    [
                        'name' => 'address3',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Address 3'),
                    ],
                    /*[
                        'name' => 'email3',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Email 3'),
                    ],
                    [
                        'name' => 'phone3',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Phone 3'),
                    ],*/
                    [
                        'name' => 'workTime3',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('contacts', 'Work time 3'),
                    ],

                    /*[
                        'name' => 'metaKeys',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('app', 'Meta keys')
                    ],*/
                    [
                        'name' => 'metaDescription',
                        'type' => FieldType::FIELD_TYPE_TEXT,
                        'label' => Yii::t('app', 'Meta description')
                    ],
                ],
                'model'         => $model,
                'form'          => $form,
            ]) ?>

            <?php /*echo $form->field($model, 'mapQ')->textInput([
                'style' => 'width: 50%;'
            ])->label(Yii::t('contacts', 'Map place')) */?>

            <?php echo $form->field($model, 'mapZoom')->dropDownList(range(0, 100), [
                'style' => 'width: 70px;'
            ])->label(Yii::t('contacts', 'Map zoom')) ?>

            <?php echo $form->field($model, 'mapApiKey')->textInput([
                'style' => 'width: 50%;'
            ])->label(Yii::t('contacts', 'Map Api Key')) ?>

            <?php echo $form->field($model, 'default')->checkbox(['value' => 1, 'label' => Yii::t('app', 'Set as default')]) ?>

            <div class="form-group <?php if ($model->hasErrors('pageId')):?>has-error<?php endif; ?>">
                <?php echo Html::label(Yii::t('contacts', 'Link with page'), 'parent-pages', [
                    'class' => 'control-label'
                ]) ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 parent-items">
                        <?php echo MenuWidget::widget([
                            'menuId' => 'parent-pages',
                            'data' => $pages,
                            'itemTemplate' => '@app/views/admin/contacts/MultiLevelMenu/form.php',
                            'itemTemplateParams' => [
                                'model' => $model
                            ],
                            'mainContainerOptions' => [
                                'levels' => [
                                    ['style' => 'margin-left: 0; padding-left: 0;'],
                                    ['style' => 'margin-left: 10px; padding-left: 10px;'],
                                ]
                            ],
                            'itemContainerOptions' => [
                                'style' => 'list-style-type: none;'
                            ],
                        ]) ?>
                    </div>
                </div>

                <?php if ($model->hasErrors('pageId')):?>
                    <?php foreach ($model->getErrors('pageId') as $error): ?>
                        <div class="help-block"><?php echo $error; ?></div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
            [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
            ]
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
