<?php

/* @var $this Itstructure\AdminModule\components\AdminView */
/* @var $model app\models\Contact */
/* @var $pages array|\yii\db\ActiveRecord[] */

$this->title = Yii::t('contacts', 'Create contact');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('contacts', 'Contacts'),
    'url' => [
        $this->params['urlPrefix'].'index'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Create');
?>
<div class="contact-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'pages' => $pages,
    ]) ?>

</div>
