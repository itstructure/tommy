<?php
use Itstructure\MFUploader\Module as MFUModule;

/* @var app\models\Product[] $products */
/* @var yii\data\Pagination $pagination */
/* @var \Itstructure\MFUploader\models\OwnerMediafile[] $ownerBanners */
?>
<div class="padding-right">

    <?php if ($ownerBanners) { ?>
        <div id="slider-carousel" class="carousel slide margin-bottom" data-ride="carousel">
            <div class="carousel-inner">
                <?php foreach ($ownerBanners as $key => $ownerBanner) { ?>
                    <div class="item <?php echo $key == 0 ? 'active' : '' ?>">
                        <img class="img-responsive" src="<?php echo $ownerBanner->mediaFile->getThumbUrl(MFUModule::THUMB_ALIAS_LARGE); ?>" alt="Баннер">
                    </div>
                <?php } ?>
            </div>
            <?php if (count($ownerBanners) > 1) { ?>
            <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
            <?php } ?>
        </div>
    <?php } ?>

    <div class="features-items">
        <h2 class="title">Все товары каталога</h2>
        <?php echo $this->render('../product/list', ['products' => $products, 'pagination' => $pagination]) ?>
    </div>
</div>
