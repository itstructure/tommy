<?php
use yii\helpers\Html;
use Itstructure\MFUploader\Module as MFUModule;
use app\models\Order;
use app\assets\{OrderAsset, BasketAsset};

/* @var array $counts */
/* @var app\models\Product[] $products */
/* @var app\models\settings\SellSetting $sellSettings */
/* @var int $total_amount */

OrderAsset::register($this);
BasketAsset::register($this);
?>
<script>
    window.need_captcha = '<?php echo Yii::t('order', 'Let us know that you are not a robot. Click right captcha pictures.'); ?>';
    window.error_captcha = '<?php echo Yii::t('order', 'Error verify captcha.'); ?>';
</script>

<form id="main_order_form" action="javascript:void(0)" method="post" class="main-order-form">
    <section id="cart_items">
        <div class="table-responsive cart_info margin-bottom">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image"><?php echo Yii::t('order', 'Image') ?></td>
                    <td class="description"><?php echo Yii::t('order', 'Title') ?></td>
                    <td class="price"><?php echo Yii::t('products', 'Price') ?>, <?php echo Yii::t('products', 'rub.') ?></td>
                    <td class="quantity"><?php echo Yii::t('order', 'Quantity') ?></td>
                    <td class="total"><?php echo Yii::t('order', 'Amount') ?>, <?php echo Yii::t('products', 'rub.') ?></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($products as $item) { ?>
                    <tr id="row_order_<?php echo $item->id; ?>">
                        <td class="cart_image">
                            <?php if ($thumbnailModel = $item->getThumbnailModel()): ?>
                                <a href="/product/<?php echo $item->alias; ?>" target="_blank">
                                    <?php echo Html::img($thumbnailModel->getThumbUrl(MFUModule::THUMB_ALIAS_SMALL), ['alt' => $item->title, 'class' => 'img-responsive']); ?>
                                </a>
                            <?php endif; ?>
                        </td>
                        <td class="cart_description">
                            <a class="cart_description_link" href="/product/<?php echo $item->alias; ?>" target="_blank"><?php echo $item->title; ?></a>
                            <!--<p><?php /*echo $item->description; */?></p>-->
                        </td>
                        <td class="cart_price">
                            <p>
                                <span id="item_price_<?php echo $item->id; ?>">
                                    <?php echo $item->price; ?>
                                </span>
                                <?php //echo Yii::t('products', 'rub.') ?>
                            </p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <?php /* ?>
                                <a class="cart_quantity_up" href=""> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
                                <a class="cart_quantity_down" href=""> - </a>
                                <?php */ ?>

                                <input role="quantity" name="quantity[<?php echo $item->id; ?>]" type="number" value="<?php echo $counts[$item->id]; ?>" id="quantity_<?php echo $item->id; ?>" class="cart_quantity_input" min="1"
                                       onchange="setCountInBasket('<?php echo $item->id; ?>', this.value)"> &nbsp;
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">
                                <span id="total_price_<?php echo $item->id; ?>">
                                    <?php echo ($item->price * $counts[$item->id]); ?>
                                </span>
                                <?php //echo Yii::t('products', 'rub.') ?>
                            </p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="javascript:void(0)" onclick="removeFromBasket('<?php echo $item->id; ?>')"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="row margin-bottom-large">
            <div class="col-sm-12 text-right">
                <div><?php echo Yii::t('order', 'Result'); ?>: <span style="font-weight: bold;" role="total_amount"><?php echo $this->params['total_amount']; ?></span> <?php echo Yii::t('products', 'rub.') ?></div>
            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-success hidden" role="alert"></div>
        </div>
    </div>

    <?php if (count($products) > 0): ?>
        <div id="send_order_block" class="row margin-bottom-large">
            <div class="col-sm-12">
                <h2 class="title text-center"><?php echo Yii::t('order', 'Checkout order'); ?></h2>
                <div data-group="name" class="form-group">
                    <input type="text" role="name" name="name" class="form-control" placeholder="<?php echo Yii::t('order', 'Name'); ?>" required>
                    <span id="help_block_name" class="help-block"></span>
                </div>
                <div data-group="email" class="form-group">
                    <input required type="email" role="email" name="email" class="form-control" placeholder="<?php echo Yii::t('order', 'Email'); ?>">
                    <span id="help_block_email" class="help-block"></span>
                </div>
                <div data-group="phone" class="form-group">
                    <input type="text" role="phone" name="phone" class="form-control" placeholder="<?php echo Yii::t('order', 'Phone'); ?>">
                    <span id="help_block_phone" class="help-block"></span>
                </div>
                <div data-group="comment" class="form-group">
                    <textarea role="comment" name="comment" class="form-control" rows="8" placeholder="<?php echo Yii::t('order', 'Comment'); ?>"></textarea>
                    <span id="help_block_comment" class="help-block"></span>
                </div>
                <div class="row delivery_methods">
                    <div class="col-sm-12">
                        <h3><?php echo Yii::t('order', 'Delivery method'); ?></h3>
                        <ul id="delivery_tabs" class="nav nav-tabs">
                            <li class="active">
                                <a href="#<?php echo Order::DELIVERY_SELF ?>" aria-controls="<?php echo Order::DELIVERY_SELF ?>" role="tab" data-toggle="tab">
                                    <input id="<?php echo Order::DELIVERY_SELF ?>_input" type="radio" name="delivery" value="<?php echo Order::DELIVERY_SELF ?>" role="delivery" checked>
                                    <label for="<?php echo Order::DELIVERY_SELF ?>_input"><?php echo Yii::t('order', 'Get by yourself'); ?></label>
                                </a>
                            </li>
                            <li>
                                <a href="#<?php echo Order::DELIVERY_POST ?>" aria-controls="<?php echo Order::DELIVERY_POST ?>" role="tab" data-toggle="tab">
                                    <input id="<?php echo Order::DELIVERY_POST ?>_input" type="radio" name="delivery" value="<?php echo Order::DELIVERY_POST ?>" role="delivery">
                                    <label for="<?php echo Order::DELIVERY_POST ?>_input"><?php echo Yii::t('order', 'Delivery'); ?></label>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="<?php echo Order::DELIVERY_SELF ?>">
                                <?php if (!empty($this->params['contacts'])): ?>
                                    <div>Забрать самостоятельно в торговых точках "Томми gifts" по адресам:</div>
                                    <?php if (!empty($this->params['contacts']->address)): ?>
                                        <div>
                                            <?php echo $this->params['contacts']->address; ?>
                                            <?php if (!empty($this->params['contacts']->workTime)): ?>
                                                (<?php echo $this->params['contacts']->workTime; ?>)
                                            <?php endif ?>
                                        </div>
                                    <?php endif ?>
                                    <?php if (!empty($this->params['contacts']->address2)): ?>
                                        <div>
                                            <?php echo $this->params['contacts']->address2; ?>
                                            <?php if (!empty($this->params['contacts']->workTime2)): ?>
                                                (<?php echo $this->params['contacts']->workTime2; ?>)
                                            <?php endif ?>
                                        </div>
                                    <?php endif ?>
                                    <?php if (!empty($this->params['contacts']->address3)): ?>
                                        <div>
                                            <?php echo $this->params['contacts']->address3; ?>
                                            <?php if (!empty($this->params['contacts']->workTime3)): ?>
                                                (<?php echo $this->params['contacts']->workTime3; ?>)
                                            <?php endif ?>
                                        </div>
                                    <?php endif ?>
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane" id="<?php echo Order::DELIVERY_POST ?>">
                                <div data-group="address" class="form-group">
                                    <textarea role="address" name="address" class="form-control" rows="4" placeholder="<?php echo Yii::t('order', 'Delivery address'); ?>"></textarea>
                                    <span id="help_block_address" class="help-block"></span>
                                    <?php if (!empty($sellSettings->deliveryConditionText)): ?>
                                        <div class="delivery-condition">
                                            <?php echo $sellSettings->deliveryConditionText; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 fl-block">
                        <div class="g-recaptcha" id="google-recaptcha-feedback"
                             data-sitekey="<?php echo Yii::$app->params['captcha']['site_key'];?>"
                             data-callback="validateRecaptchaOrder"
                             data-expired-callback="grecaptcha_reset"
                        ></div>
                    </div>
                    <div class="col-md-6 fl-block j-e">
                        <button type="button" role="send" class="btn btn-primary pull-right"><?php echo Yii::t('app', 'Send'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</form>
