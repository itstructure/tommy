<?php

return [
    'Pages' => 'Pages',
    'Create page' => 'Create page',
    'Update page' => 'Update page',
    'View products' => 'View products',
    'Neutral' => 'Neutral',
    'Top menu' => 'Top menu',
    'Footer 1' => 'Footer 1',
    'Footer 2' => 'Footer 2',
    'Order' => 'Order',
    'Position' => 'Position',
    'Above' => 'Above',
    'Below' => 'Below',
];