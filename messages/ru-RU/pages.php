<?php

return [
    'Pages' => 'Страницы',
    'Create page' => 'Создать страницу',
    'Update page' => 'Обновить страницу',
    'View products' => 'Посмотреть продукцию',
    'Neutral' => 'Нейтрально',
    'Top menu' => 'Верхнее меню',
    'Footer 1' => 'Футер 1',
    'Footer 2' => 'Футер 2',
    'Order' => 'Порядок',
    'Position' => 'Расположение',
    'Above' => 'Выше',
    'Below' => 'Ниже',
];