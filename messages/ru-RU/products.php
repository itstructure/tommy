<?php

return [
    'Products' => 'Товары',
    'Create product' => 'Создать товар',
    'Update product' => 'Обновить товар',
    'Parent page' => 'Родительская страница',
    'Parent category' => 'Родительская категория',
    'Posted' => 'Опубликовано',
    'Price' => 'Стоимость',
    'rub.' => 'руб.',
    'General status' => 'Общий статус',
    'Regular' => 'Обычный',
    'Novelty' => 'Новинка',
    'Sale' => 'Распродажа',
];