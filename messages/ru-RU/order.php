<?php

return [
    'Name' => 'Имя',
    'Email' => 'Почта',
    'Phone' => 'Телефон',
    'Comment' => 'Комментарий',
    'Address' => 'Адрес',
    'Checkout order' => 'Оформить заказ',
    'You have successfully sent your order message.' => 'Вы успешно отправили свой заказ.',
    'The goods will be sent to the address you provided.' => 'Товар будет отправлен на указанный вами адрес.',
    'Then you can get the goods at these addresses' => 'Далее вы можете забрать товар по данным адресам',
    'The manager will contact you.' => 'С вами свяжется менеджер.',
    'Let us know that you are not a robot. Click right captcha pictures.' => 'Подтвердите что вы не робот. Выберите правильно картинки на каптче.',
    'Error verify captcha.' => 'Ошибка проверки каптчи.',
    'New client' => 'Новый клиент',
    'New order' => 'Новый заказ',
    'New order in {project_name} site.' => 'Новый заказ на сайте {project_name}.',
    'Good' => 'Товар',
    'Title' => 'Название',
    'Quantity' => 'Количество',
    'Amount' => 'Сумма',
    'Result' => 'Итог',
    'Image' => 'Изоображение',
    'Delivery' => 'Доставка',
    'Delivery method' => 'Способ доставки',
    'Get by yourself' => 'Заберём самостоятельно',
    'Receive by post' => 'Получить почтой',
    'Delivery address' => 'Адрес доставки',
    'Must fill delivery address' => 'Необходимо заполнить "Адрес доставки"',
    'Must fill phone' => 'Необходимо заполнить "телефон"',
    'Delivery condition text' => 'Условия доставки',
];