<?php

return [
    'project_name' => 'Tommy',
    'defaultPageSize' => 36,
    'adminEmail' => 'tommygifts@mail.ru',
    'bccAdminEmail' => 'www.to.business@gmail.com',
    'captcha' => require __DIR__ . '/captcha.php'
];
