<?php

use Itstructure\AdminModule\Module as AdminModule;
use Itstructure\MFUploader\Module as MFUModule;
use Itstructure\MFUploader\components\LocalUploadComponent;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$translations = require __DIR__ . '/translations.php';
$baseUrl = require __DIR__ . '/base-url.php';

$config = [
    'id' => 'yii2_template_simple_console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'catalogParse' => [
            'class' => app\components\CatalogParseComponent::class,
            'sourceDir' => dirname($_SERVER['SCRIPT_FILENAME']) . DIRECTORY_SEPARATOR . 'tmp',
            'consoleLogging' => true,
            'uploadComponentName' => 'local-upload-component',
            'parentModelClass' => app\models\Category::class,
            'childModelClass' => app\models\Product::class,
            'parentModelAdditionAttributes' => [
                'active' => 1
            ],
            'childModelAdditionAttributes' => [
                'active' => 1,
            ],
            'folder' => 'catalog'
        ],
        'imageRebuild' => [
            'class' => app\components\ImageRebuildComponent::class,
            'consoleLogging' => true,
            'uploadComponentName' => 'local-upload-component'
        ],
        'local-upload-component' => [
            'class' => LocalUploadComponent::class,
            'checkExtensionByMimeType' => false,
            'uploadRoot' => dirname($_SERVER['SCRIPT_FILENAME']) . DIRECTORY_SEPARATOR . 'web',
            'thumbsConfig' => [
                MFUModule::THUMB_ALIAS_DEFAULT => [
                    'name' => 'Default size',
                    'size' => [200, null],
                ],
                MFUModule::THUMB_ALIAS_SMALL => [
                    'name' => 'Small size',
                    'size' => [100, null],
                ],
                MFUModule::THUMB_ALIAS_MEDIUM => [
                    'name' => 'Medium size',
                    'size' => [470, null],
                ],
                MFUModule::THUMB_ALIAS_LARGE => [
                    'name' => 'Large size',
                    'size' => [970, null],
                ],
            ]
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'hostInfo' => $baseUrl,
            'scriptUrl' => $baseUrl,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'i18n' => [
            'translations' => $translations
        ],
    ],
    'params' => $params,
    'modules' => [
        'admin' => [
            'class' => AdminModule::class,
        ],
        'mfuploader' => [
            'class' => MFUModule::class,
        ],
    ],
    'controllerMap' => [
        'sitemap' => [
            'class' => 'Itstructure\Sitemap\SitemapController',
            'baseUrl' => $baseUrl,
            'modelsPath' => '@app/models/sitemap', // Sitemap-data models directory
            'modelsNamespace' => 'app\models\sitemap', // Namespace in [[modelsPath]] files
            'savePathAlias' => '@app/web', // Where would be placed the generated sitemap-files
            'sitemapFileName' => 'sitemap.xml', // Name of main sitemap-file in [[savePathAlias]] directory
        ],
        /*'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],*/
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
